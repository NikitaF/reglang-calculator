package ru.appmat.toc.regular

import io.mockk.every
import io.mockk.mockkObject
import kotlin.test.Test
import kotlin.test.assertEquals

typealias RightRegularGrammar = RegularGrammar.RightRegularGrammar
typealias RightRule = RegularGrammarRule.NonTerminalRule.RightRule

class RightRegularGrammarTests {

    @Test
    fun unionRightGrammarWithAnotherContainingOnlyLetterRules() {
        val grammarA = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    RightRule('a', NonTerminalSymbol('S', 0u)),
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    LetterRule('a')
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    LetterRule('b')
                )
            )
        )

        val grammarB = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LetterRule('a'),
                    LetterRule('b'),
                    LetterRule('c'),
                    LetterRule('d'),
                    LetterRule('e')
                )
            )
        )

        assertEquals(
            RightRegularGrammar(
                NonTerminalSymbol('S', 0u),
                mapOf(
                    NonTerminalSymbol('S', 0u) to setOf(
                        RightRule('a', NonTerminalSymbol('A', 0u)),
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('a'),
                        LetterRule('b'),
                        LetterRule('c'),
                        LetterRule('d'),
                        LetterRule('e')
                    ),
                    NonTerminalSymbol('A', 0u) to setOf(
                        RightRule('a', NonTerminalSymbol('A', 0u)),
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('a')
                    ),
                    NonTerminalSymbol('A', 1u) to setOf(
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('b')
                    ),
                    NonTerminalSymbol('B', 0u) to setOf(
                        LetterRule('a'),
                        LetterRule('b'),
                        LetterRule('c'),
                        LetterRule('d'),
                        LetterRule('e')
                    )
                )
            ),
            grammarA union grammarB
        )
    }

    @Test
    fun unionRightGrammarWithAnother() {
        val grammarA = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    RightRule('a', NonTerminalSymbol('S', 0u)),
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    LetterRule('a')
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    LetterRule('b')
                )
            )
        )

        val grammarB = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    RightRule('c', NonTerminalSymbol('S', 0u)),
                    RightRule('d', NonTerminalSymbol('D', 0u)),
                    LetterRule('c')
                ),
                NonTerminalSymbol('D', 0u) to setOf(
                    RightRule('d', NonTerminalSymbol('D', 0u)),
                    LetterRule('d')
                )
            )
        )

        assertEquals(
            RightRegularGrammar(
                NonTerminalSymbol('S', 0u),
                mapOf(
                    NonTerminalSymbol('S', 0u) to setOf(
                        RightRule('a', NonTerminalSymbol('A', 0u)),
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        RightRule('c', NonTerminalSymbol('B', 0u)),
                        RightRule('d', NonTerminalSymbol('B', 1u)),
                        LetterRule('a'),
                        LetterRule('c')
                    ),
                    NonTerminalSymbol('A', 0u) to setOf(
                        RightRule('a', NonTerminalSymbol('A', 0u)),
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('a')
                    ),
                    NonTerminalSymbol('A', 1u) to setOf(
                        RightRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('b')
                    ),
                    NonTerminalSymbol('B', 0u) to setOf(
                        RightRule('c', NonTerminalSymbol('B', 0u)),
                        RightRule('d', NonTerminalSymbol('B', 1u)),
                        LetterRule('c')
                    ),
                    NonTerminalSymbol('B', 1u) to setOf(
                        RightRule('d', NonTerminalSymbol('B', 1u)),
                        LetterRule('d')
                    )
                )
            ),
            grammarA union grammarB
        )
    }

    @Test
    fun unionRightGrammarWithItself() {
        val b = NonTerminalSymbol('B')
        val a = NonTerminalSymbol('A')

        val rightRegularGrammar = RightRegularGrammar(
            a,
            mapOf(
                a to setOf(
                    RightRule('b', b),
                    LetterRule('c')
                ),
                b to setOf(
                    RightRule('a', a)
                )
            )
        )
        val s = NonTerminalSymbol('S')

        assertEquals(
            RightRegularGrammar(
                s,
                mapOf(
                    s to setOf(
                        RightRule('b', a.copy(index = 1u)),
                        RightRule('b', b.copy(index = 1u)),
                        LetterRule('c')
                    ),
                    a to setOf(
                        RightRule('b', a.copy(index = 1u)),
                        LetterRule('c')
                    ),
                    b to setOf(
                        RightRule('b', b.copy(index = 1u)),
                        LetterRule('c')
                    ),
                    a.copy(index = 1u) to setOf(
                        RightRule('a', a)
                    ),
                    b.copy(index = 1u) to setOf(
                        RightRule('a', b)
                    )
                )
            ),
            rightRegularGrammar union rightRegularGrammar
        )
    }

    @Test
    fun toLambdaFreeCommonCase() {
        val s = NonTerminalSymbol('S')
        val a = NonTerminalSymbol('A')
        val b = NonTerminalSymbol('B')
        val c = NonTerminalSymbol('C')

        val rightRegularGrammar = RightRegularGrammar(
            s,
            mapOf(
                s to setOf(
                    RightRule('b', a),
                    RightRule('a', b),
                    RightRule('b', c),
                    EmptyRule
                ),
                a to setOf(
                    RightRule('a', s),
                    RightRule('a', c),
                    EmptyRule
                ),
                b to setOf(
                    LetterRule('a'),
                    EmptyRule
                ),
                c to setOf(
                    EmptyRule
                )
            )
        )

        assertEquals(
            RightRegularGrammar(
                s,
                mapOf(
                    s to setOf(
                        RightRule('b', a),
                        RightRule('a', b),
                        EmptyRule,
                        LetterRule('b'),
                        LetterRule('a')
                    ),
                    a to setOf(
                        RightRule('a', s),
                        LetterRule('a')
                    ),
                    b to setOf(
                        LetterRule('a')
                    )
                )
            ), rightRegularGrammar.toLambdaFree()
        )
    }

//region plus-closure
    /**
     * Without Lambda
     * S->aA|bB|cC          S->aA|bB|cC
     * A->b         ----->  A->bS|b
     * B->c         ----->  B->cS|c
     * C->a                 C->aS|a
     */
    @ExperimentalUnsignedTypes
    @Test
    fun plusClosureWithoutLambda() {
        val s = NonTerminalSymbol('S', 1u)
        val a = NonTerminalSymbol('A', 1u)
        val b = NonTerminalSymbol('B', 1u)
        val c = NonTerminalSymbol('C', 1u)
        val train = RightRegularGrammar(
            s,
            mapOf(
                s to setOf(
                    RightRule('a', a.copy(index = 1u)),
                    RightRule('b', b.copy(index = 1u)),
                    RightRule('c', c.copy(index = 1u))
                ),
                a to setOf(
                    LetterRule('b')
                ),
                b to setOf(
                    LetterRule('c')
                ),
                c to setOf(
                    LetterRule('a')
                )
            )
        )
        val test = RightRegularGrammar(
            s,
            mapOf(
                s to setOf(
                    RightRule('a', a.copy(index = 1u)),
                    RightRule('b', b.copy(index = 1u)),
                    RightRule('c', c.copy(index = 1u))
                ),
                a to setOf(
                    LetterRule('b'),
                    RightRule('b', s.copy(index = 1u))
                ),
                b to setOf(
                    LetterRule('c'),
                    RightRule('c', s.copy(index = 1u))
                ),
                c to setOf(
                    LetterRule('a'),
                    RightRule('a', s.copy(index = 1u))
                )
            )
        )
        mockkObject(train)
        every { train.toLambdaFree() } returns train
        assertEquals(test, train.plusClosure())
    }

    /**
     * With Lambda
     * S->aA|bB|cC|Lambda           S->aA|bB|cC|Lambda
     * A->b                 ----->  A->bS|b
     * B->c                 ----->  B->cS|c
     * C->a                         C->aS|a
     */
    @ExperimentalUnsignedTypes
    @Test
    fun plusClosureWithLambda() {
        val s = NonTerminalSymbol('S')
        val a = NonTerminalSymbol('A')
        val b = NonTerminalSymbol('B')
        val c = NonTerminalSymbol('C')
        val train = RightRegularGrammar(
            s,
            mapOf(
                s to setOf(
                    RightRule('a', a.copy()),
                    RightRule('b', b.copy()),
                    RightRule('c', c.copy()),
                    RegularGrammarRule.EmptyRule
                ),
                a to setOf(
                    LetterRule('b')
                ),
                b to setOf(
                    LetterRule('c')
                ),
                c to setOf(
                    LetterRule('a')
                )
            )
        )
        val test = RightRegularGrammar(
            s,
            mapOf(
                s to setOf(
                    RightRule('a', a.copy()),
                    RightRule('b', b.copy()),
                    RightRule('c', c.copy()),
                    RegularGrammarRule.EmptyRule
                ),
                a to setOf(
                    LetterRule('b'),
                    RightRule('b', s.copy())
                ),
                b to setOf(
                    LetterRule('c'),
                    RightRule('c', s.copy())
                ),
                c to setOf(
                    LetterRule('a'),
                    RightRule('a', s.copy())
                )
            )
        )
        mockkObject(train)
        every { train.toLambdaFree() } returns train
        assertEquals(test, train.plusClosure())
    }
//endregion
//region star-closure
    /**
     * Without Lambda
     * S->aA|bB|cC          S->aA|bB|cC
     * A->b         ----->  A->bS|b
     * B->c         ----->  B->cS|c
     * C->a                 C->aS|a
     *                      S1->aA|bB|cC|Lambda
     */
    @ExperimentalUnsignedTypes
    @Test
    fun starClosureWithoutLambda() {
        val s = NonTerminalSymbol('S')
        val a = NonTerminalSymbol('A')
        val b = NonTerminalSymbol('B')
        val c = NonTerminalSymbol('C')
        val train = RightRegularGrammar(
            s,
            mapOf(
                s to setOf(
                    RightRule('a', a.copy()),
                    RightRule('b', b.copy()),
                    RightRule('c', c.copy())
                ),
                a to setOf(
                    LetterRule('b')
                ),
                b to setOf(
                    LetterRule('c')
                ),
                c to setOf(
                    LetterRule('a')
                )
            )
        )
        val b0 = NonTerminalSymbol('B', 0u)
        val b1 = NonTerminalSymbol('B', 1u)
        val b2 = NonTerminalSymbol('B', 2u)
        val b3 = NonTerminalSymbol('B', 3u)
        val b4 = NonTerminalSymbol('B', 4u)
        val test = RightRegularGrammar(
            b4,
            mapOf(
                b0 to setOf(
                    RightRule('a', b1.copy()),
                    RightRule('b', b2.copy()),
                    RightRule('c', b3.copy())
                ),
                b1 to setOf(
                    LetterRule('b'),
                    RightRule('b', b0.copy())
                ),
                b2 to setOf(
                    LetterRule('c'),
                    RightRule('c', b0.copy())
                ),
                b3 to setOf(
                    LetterRule('a'),
                    RightRule('a', b0.copy())
                ),
                b4 to setOf(
                    RightRule('a', b1.copy()),
                    RightRule('b', b2.copy()),
                    RightRule('c', b3.copy()),
                    RegularGrammarRule.EmptyRule
                )
            )
        )
        mockkObject(train)
        every { train.toLambdaFree() } returns train
        assertEquals(test, train.starClosure())
    }

    /**
     * With Lambda
     * S->aA|bB|cC|Lambda           S->aA|bB|cC|Lambda
     * A->b                 ----->  A->bS|b
     * B->c                 ----->  B->cS|c
     * C->a                         C->aS|a
     */
    @ExperimentalUnsignedTypes
    @Test
    fun starClosureWithLambda() {
        val s = NonTerminalSymbol('S', 1u)
        val a = NonTerminalSymbol('A', 1u)
        val b = NonTerminalSymbol('B', 1u)
        val c = NonTerminalSymbol('C', 1u)
        val train = RightRegularGrammar(
            s,
            mapOf(
                s to setOf(
                    RightRule('a', a.copy(index = 1u)),
                    RightRule('b', b.copy(index = 1u)),
                    RightRule('c', c.copy(index = 1u)),
                    RegularGrammarRule.EmptyRule
                ),
                a to setOf(
                    LetterRule('b')
                ),
                b to setOf(
                    LetterRule('c')
                ),
                c to setOf(
                    LetterRule('a')
                )
            )
        )
        val test = RightRegularGrammar(
            s,
            mapOf(
                s to setOf(
                    RightRule('a', a.copy(index = 1u)),
                    RightRule('b', b.copy(index = 1u)),
                    RightRule('c', c.copy(index = 1u)),
                    RegularGrammarRule.EmptyRule
                ),
                a to setOf(
                    LetterRule('b'),
                    RightRule('b', s.copy(index = 1u))
                ),
                b to setOf(
                    LetterRule('c'),
                    RightRule('c', s.copy(index = 1u))
                ),
                c to setOf(
                    LetterRule('a'),
                    RightRule('a', s.copy(index = 1u))
                )
            )
        )
        mockkObject(train)
        every { train.toLambdaFree() } returns train
        assertEquals(test, train.starClosure())
    }
//endregion

    @Test
    fun `concatenation of RightRegularGrammars without LAMBDA in language`() {
        val grammarA = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    RightRule('a', NonTerminalSymbol('S', 0u)),
                    RightRule('e', NonTerminalSymbol('E', 0u))
                ),
                NonTerminalSymbol('E', 0u) to setOf(
                    RightRule('e', NonTerminalSymbol('E', 0u)),
                    LetterRule('b'),
                    LetterRule('c')
                )
            )
        )

        val grammarB = RightRegularGrammar(
            NonTerminalSymbol('T', 0u),
            mapOf(
                NonTerminalSymbol('T', 0u) to setOf(
                    RightRule('t', NonTerminalSymbol('T', 0u)),
                    RightRule('f', NonTerminalSymbol('F', 0u))
                ),
                NonTerminalSymbol('F', 0u) to setOf(
                    RightRule('f', NonTerminalSymbol('F', 0u)),
                    LetterRule('f')
                )
            )
        )

        val grammarAB = RightRegularGrammar(
            NonTerminalSymbol('A', 0u),
            mapOf(
                NonTerminalSymbol('A', 0u) to setOf(
                    RightRule('a', NonTerminalSymbol('A', 0u)),
                    RightRule('e', NonTerminalSymbol('A', 1u))
                ),
                NonTerminalSymbol('A', 1u) to setOf(
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    RightRule('c', NonTerminalSymbol('B', 0u)),
                    RightRule('e', NonTerminalSymbol('A', 1u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    RightRule('t', NonTerminalSymbol('B', 0u)),
                    RightRule('f', NonTerminalSymbol('B', 1u))
                ),
                NonTerminalSymbol('B', 1u) to setOf(
                    RightRule('f', NonTerminalSymbol('B', 1u)),
                    LetterRule('f')
                )
            )
        )
        mockkObject(grammarA)
        every { grammarA.toLambdaFree() } returns grammarA

        assertEquals(grammarAB, grammarA.concatenate(grammarB))
    }

    @Test
    fun `concatenation of RightRegularGrammars with LAMBDA in language`() {
        val grammarA = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    RightRule('a', NonTerminalSymbol('S', 0u)),
                    RightRule('e', NonTerminalSymbol('E', 0u)),
                    EmptyRule
                ),
                NonTerminalSymbol('E', 0u) to setOf(
                    RightRule('e', NonTerminalSymbol('E', 0u)),
                    LetterRule('b'),
                    LetterRule('c')
                )
            )
        )
        val grammarAWithoutLambda = RightRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    RightRule('a', NonTerminalSymbol('S', 0u)),
                    RightRule('e', NonTerminalSymbol('E', 0u)),
                    EmptyRule
                ),
                NonTerminalSymbol('E', 0u) to setOf(
                    RightRule('e', NonTerminalSymbol('E', 0u)),
                    LetterRule('b'),
                    LetterRule('c')
                )
            )
        )

        val grammarB = RightRegularGrammar(
            NonTerminalSymbol('T', 0u),
            mapOf(
                NonTerminalSymbol('T', 0u) to setOf(
                    RightRule('t', NonTerminalSymbol('T', 0u)),
                    RightRule('f', NonTerminalSymbol('F', 0u)),
                    EmptyRule
                ),
                NonTerminalSymbol('F', 0u) to setOf(
                    RightRule('f', NonTerminalSymbol('F', 0u)),
                    LetterRule('f')
                )
            )
        )

        val grammarAB = RightRegularGrammar(
            NonTerminalSymbol('A', 0u),
            mapOf(
                NonTerminalSymbol('A', 0u) to setOf(
                    RightRule('a', NonTerminalSymbol('A', 0u)),
                    RightRule('e', NonTerminalSymbol('A', 1u)),
                    EmptyRule
                ),
                NonTerminalSymbol('A', 1u) to setOf(
                    RightRule('b', NonTerminalSymbol('B', 0u)),
                    RightRule('c', NonTerminalSymbol('B', 0u)),
                    RightRule('e', NonTerminalSymbol('A', 1u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    RightRule('t', NonTerminalSymbol('B', 0u)),
                    RightRule('f', NonTerminalSymbol('B', 1u)),
                    EmptyRule
                ),
                NonTerminalSymbol('B', 1u) to setOf(
                    RightRule('f', NonTerminalSymbol('B', 1u)),
                    LetterRule('f')
                )
            )
        )
        mockkObject(grammarA)
        every { grammarA.toLambdaFree() } returns grammarAWithoutLambda

        assertEquals(grammarAB, grammarA.concatenate(grammarB))
    }

    @Test
    fun `RightRegularGrammar to NFA #1`() {

        val grammarA = RightRegularGrammar(
                NonTerminalSymbol('S', 0u),
                mapOf(
                        NonTerminalSymbol('S', 0u) to setOf(
                                RightRule('a', NonTerminalSymbol('S', 0u)),
                                RightRule('b', NonTerminalSymbol('B', 0u)),
                                LetterRule('a')
                        ),
                        NonTerminalSymbol('B', 0u) to setOf(
                                RightRule('b', NonTerminalSymbol('B', 0u)),
                                LetterRule('b')
                        )
                )
        )

        val transitionsTable: List<Map<Char, Set<Int>>> = listOf(
            mapOf(
                    'a' to setOf(0,2),
                    'b' to setOf(1)
            ),
            mapOf(
                    'b' to setOf(1,3)
            )
        )

        val stateCount: Int = 4
        val startIndex: Int = 0
        val terminalStates: Set<Int> = setOf(2,3)

        val expectedAutoA = FiniteAutomaton.NFA(stateCount,transitionsTable,startIndex,terminalStates)

        assertEquals(grammarA.toNFA(), expectedAutoA)
    }

    @Test
    fun `RightRegularGrammar to NFA #2`() {

        val grammarB = RightRegularGrammar(
                NonTerminalSymbol('S', 0u),
                mapOf(
                        NonTerminalSymbol('S', 0u) to setOf(
                                LetterRule('a'),
                                LetterRule('b'),
                                LetterRule('c'),
                                LetterRule('d'),
                                LetterRule('e')
                        )
                )
        )

        val transitionsTable: List<Map<Char, Set<Int>>> = listOf(
                mapOf(
                        'a' to setOf(1),
                        'b' to setOf(2),
                        'c' to setOf(3),
                        'd' to setOf(4),
                        'e' to setOf(5)
                )
        )

        val stateCount: Int = 6
        val startIndex: Int = 0
        val terminalStates: Set<Int> = setOf(1,2,3,4,5)

        val expectedAutoB = FiniteAutomaton.NFA(stateCount,transitionsTable,startIndex,terminalStates)

        assertEquals(grammarB.toNFA(), expectedAutoB)
    }
}
