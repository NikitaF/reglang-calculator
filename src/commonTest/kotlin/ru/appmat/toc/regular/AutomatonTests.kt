package ru.appmat.toc.regular

import io.mockk.every
import io.mockk.mockkConstructor
import io.mockk.mockkObject
import kotlin.test.*

class AutomatonTests {

    // dfa-basic tests
    @Test
    fun `DFA initialization`() {
        //Количество букв b по модулю 3 было равно 2.

        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()

        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)
        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )

        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))
    }

    @Test
    fun `DFA copy`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)
        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )
        val automata2 = automata1.copy()
        assertEquals(automata1, automata2)
    }

    @Test
    fun `Transitions Table does not have any transition`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val exception = assertFailsWith<IllegalArgumentException> {
            val automata1 = FiniteAutomaton.DFA(
                stateCount,
                a,
                transitionsTable,
                startState,
                terminalStates
            )
        }

    }

    @Test
    fun `Transitions Table has transition by symbol that are not in alphabet`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2,
                'c' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val exception = assertFailsWith<IllegalArgumentException> {
            val automata1 = FiniteAutomaton.DFA(
                stateCount,
                a,
                transitionsTable,
                startState,
                terminalStates
            )
        }

    }

    @Test
    fun `Transitions Table has transitions to a nonexistent state`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 4
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val exception = assertFailsWith<IllegalArgumentException> {
            val automata1 = FiniteAutomaton.DFA(
                stateCount,
                a,
                transitionsTable,
                startState,
                terminalStates
            )
        }

    }

    @Test
    fun `withAlphabet method does not contain DFA alphabet`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )
        val a2 = setOf<Letter>('a', 'c', 'd')
        assertFailsWith<IllegalArgumentException> {
            val automata2 = automata1.withAlphabet(a2)
        }
        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))

    }

    @Test
    fun `withAlphabet method equal DFA alphabet`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )
        val a2 = setOf<Letter>('a', 'b')
        val automata2 = automata1.withAlphabet(a2)

        assertEquals(automata1, automata2)
        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))

    }

    @Test
    fun `withAlphabet method`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )
        val a2 = setOf<Letter>('a', 'b', 'c')
        val automata2 = automata1.withAlphabet(a2)

        assertNotEquals(automata1, automata2)
        assertEquals(automata1.stateCount + 1, automata2.stateCount)

        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))

        assertTrue(automata2.check("baaba"))
        assertTrue(automata2.check("bbbbb"))
        assertTrue(!automata2.check("bbddebbb"))

    }

    @Test
    fun `complement method`() {
        //Количество букв b по модулю 3 было равно 2.
        val a: CharAlphabet = setOf<Letter>('a', 'b')

        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )

        val automata2 = automata1.complement()

        assertNotEquals(automata1, automata2)
        assertEquals(automata1.stateCount, automata2.stateCount)
        assertEquals(automata2.terminalStates, setOf(0, 1))

        assertTrue(automata1.check("baaba"))
        assertTrue(automata1.check("bbbbb"))
        assertTrue(!automata1.check("bbddebbb"))

        assertTrue(!automata2.check("baaba"))
        assertTrue(!automata2.check("bbbbb"))
        assertTrue(automata2.check("bbbba"))
        assertTrue(!automata2.check("bbddebbb"))

    }
    //endregion dfa-basic tests


    //dfa-deleteUnreachableStates tests
    @Test
    fun `deleteUnreachableStates first test`() {
        //Количество букв b по модулю 3 было равно 2.

        val a: CharAlphabet = setOf<Letter>('a', 'b')


        val transitionsTable: List<Map<Char, Int>> = listOf(

            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 3
            ),
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 3,
                'b' to 4
            ),
            mapOf(
                'a' to 4,
                'b' to 1
            ),
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 0,
                'b' to 1
            )
        )

        val stateCount: Int = transitionsTable.count()

        val startState: Int = 1
        val terminalStates: Set<Int> = setOf(4)
        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )


        val transitionsTable2: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )

        val stateCount2: Int = transitionsTable2.count()

        val startState2: Int = 0
        val terminalStates2: Set<Int> = setOf(2)

        val automata2 = FiniteAutomaton.DFA(
            stateCount2,
            a,
            transitionsTable2,
            startState2,
            terminalStates2
        )


        val automata1DeletedUnreachable = automata1.deleteUnreachableStates()



        assertEquals(automata1DeletedUnreachable, automata2)

    }

    @Test
    fun `deleteUnreachableStates  second test`() {
        //автомат принимает а или б или пустое слово

        val a: CharAlphabet = setOf<Letter>('a', 'b', 'c')


        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 2,
                'b' to 2,
                'c' to 0
            ),
            mapOf(
                'a' to 0,
                'b' to 1,
                'c' to 2
            ),
            mapOf(
                'a' to 4,
                'b' to 4,
                'c' to 2
            ),
            mapOf(
                'a' to 0,
                'b' to 1,
                'c' to 2
            ),
            mapOf(
                'a' to 4,
                'b' to 4,
                'c' to 4
            ),
            mapOf(
                'a' to 0,
                'b' to 1,
                'c' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 2,
                'c' to 2
            )
        )

        val stateCount: Int = transitionsTable.count()

        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(0, 2)
        val automata1 = FiniteAutomaton.DFA(
            stateCount,
            a,
            transitionsTable,
            startState,
            terminalStates
        )
        val automata1DeletedUnreachable = automata1.deleteUnreachableStates()

        val transitionsTable2: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 1,
                'b' to 1,
                'c' to 0
            ),
            mapOf(
                'a' to 2,
                'b' to 2,
                'c' to 1
            ),
            mapOf(
                'a' to 2,
                'b' to 2,
                'c' to 2
            )
        )
        val stateCount2: Int = transitionsTable2.count()
        val startState2: Int = 0
        val terminalStates2: Set<Int> = setOf(0, 1)
        val automata2 = FiniteAutomaton.DFA(
            stateCount2,
            a,
            transitionsTable2,
            startState2,
            terminalStates2
        )

        assertEquals(automata1DeletedUnreachable, automata2)

    }
    // endregion dfa-deleteUnreachableStates tests


    //dfa-intersection tests
    private fun createFirstDFA(): FiniteAutomaton.DFA {
        val alphabet1: CharAlphabet = setOf('0', '1')
        val startState1 = 0
        val terminalStates1: Set<Int> = setOf(1)
        val transitionsTable1: List<Map<Char, Int>> = listOf(
            mapOf(
                '0' to 0,
                '1' to 1
            ),
            mapOf(
                '0' to 1,
                '1' to 1
            )
        )
        val stateCount1 = transitionsTable1.count()
        return FiniteAutomaton.DFA(stateCount1, alphabet1, transitionsTable1, startState1, terminalStates1)
    }

    private fun createSecondDFA(): FiniteAutomaton.DFA {
        val alphabet2: CharAlphabet = setOf('0', '1')
        val startState2 = 0
        val terminalStates2: Set<Int> = setOf(2, 3)
        val transitionsTable2: List<Map<Char, Int>> = listOf(
            mapOf(
                '0' to 1,
                '1' to 0
            ),
            mapOf(
                '0' to 1,
                '1' to 2
            ),
            mapOf(
                '0' to 2,
                '1' to 2
            ),
            mapOf(
                '0' to 3,
                '1' to 3
            )
        )
        val stateCount2 = transitionsTable2.count()
        return FiniteAutomaton.DFA(stateCount2, alphabet2, transitionsTable2, startState2, terminalStates2)
    }

    /**
     * Пример с wiki: @see https://neerc.ifmo.ru/wiki/index.php?title=Прямое_произведение_ДКА
     */
    @Test
    fun `Intersection test`() {
        val dfa1 = createFirstDFA()
        val dfa2 = createSecondDFA()

        // expected intersected dfa
        val alphabet3: CharAlphabet = setOf('0', '1')
        val startState3 = 0
        val terminalStates3: Set<Int> = setOf(6, 7)
        val transitionsTable3: List<Map<Char, Int>> = listOf(
            mapOf(
                '0' to 1,
                '1' to 4
            ),
            mapOf(
                '0' to 1,
                '1' to 6
            ),
            mapOf(
                '0' to 2,
                '1' to 6
            ),
            mapOf(
                '0' to 3,
                '1' to 7
            ),
            mapOf(
                '0' to 5,
                '1' to 4
            ),
            mapOf(
                '0' to 5,
                '1' to 6
            ),
            mapOf(
                '0' to 6,
                '1' to 6
            ),
            mapOf(
                '0' to 7,
                '1' to 7
            )
        )
        val stateCount3 = transitionsTable3.count()
        val expectedIntersectedDFA =
            FiniteAutomaton.DFA(stateCount3, alphabet3, transitionsTable3, startState3, terminalStates3)

        val intersectedDFA = dfa1.intersect(dfa2)
        val intersectedDFA2 = dfa2.intersect(dfa1)
        assertEquals(expectedIntersectedDFA, intersectedDFA)
        assertEquals(expectedIntersectedDFA, intersectedDFA2)

        assertFalse(intersectedDFA.check("1"))
        assertFalse(intersectedDFA.check("1111"))
        assertFalse(intersectedDFA.check("0000"))
        assertTrue(intersectedDFA.check("01"))
        assertTrue(intersectedDFA.check("001"))
        assertTrue(intersectedDFA.check("0011"))
    }

    /**
     * Пример объединения с wiki: @see https://neerc.ifmo.ru/wiki/index.php?title=Прямое_произведение_ДКА
     */
    @Test
    fun `Union test`() {
        val dfa1 = createFirstDFA()
        val dfa2 = createSecondDFA()

        // expected intersected dfa
        val alphabet3: CharAlphabet = setOf('0', '1')
        val startState3 = 0
        val terminalStates3: Set<Int> = setOf(2, 3, 4, 5, 6, 7)
        val transitionsTable3: List<Map<Char, Int>> = listOf(
            mapOf(
                '0' to 1,
                '1' to 4
            ),
            mapOf(
                '0' to 1,
                '1' to 6
            ),
            mapOf(
                '0' to 2,
                '1' to 6
            ),
            mapOf(
                '0' to 3,
                '1' to 7
            ),
            mapOf(
                '0' to 5,
                '1' to 4
            ),
            mapOf(
                '0' to 5,
                '1' to 6
            ),
            mapOf(
                '0' to 6,
                '1' to 6
            ),
            mapOf(
                '0' to 7,
                '1' to 7
            )
        )
        val stateCount3 = transitionsTable3.count()
        val expectedUnitedDFA =
            FiniteAutomaton.DFA(stateCount3, alphabet3, transitionsTable3, startState3, terminalStates3)


        val unitedDFA = dfa1.union(dfa2)
        val unitedDFA2 = dfa2.union(dfa1)
        assertEquals(expectedUnitedDFA, unitedDFA)
        assertEquals(expectedUnitedDFA, unitedDFA2)

        assertTrue(unitedDFA.check("1"))
        assertTrue(unitedDFA.check("1111"))
        assertTrue(unitedDFA.check("01"))
        assertTrue(unitedDFA.check("001"))
        assertTrue(unitedDFA.check("0011"))
        assertFalse(unitedDFA.check("0"))
        assertFalse(unitedDFA.check("00"))
        assertFalse(unitedDFA.check("000"))
    }
    
    @Test
    fun `toNFA  first test`() {
        val a: CharAlphabet = setOf<Letter>('a', 'b')
        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 1,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(2)

        val set0: Set<Int> = setOf(0)
        val set1: Set<Int> = setOf(1)
        val set2: Set<Int> = setOf(2)
        val transitionsTableNFA: List<Map<Char, Set<Int>>> = listOf(
            mapOf(
                'a' to set0,
                'b' to set1
            ),
            mapOf(
                'a' to set1,
                'b' to set2
            ),
            mapOf(
                'a' to set2,
                'b' to set0
            )
        )
        val stateCountNFA: Int = transitionsTableNFA.count()
        val startStateNFA: Int = 0
        val terminalStatesNFA: Set<Int> = setOf(2)

        val myDFA = FiniteAutomaton.DFA(
            stateCount, a, transitionsTable,
            startState, terminalStates)
        val auNFA = myDFA.toNFA()

        val expectedNFA = FiniteAutomaton.NFA(
            stateCountNFA, transitionsTableNFA,
            startStateNFA, terminalStatesNFA)
        assertEquals(auNFA, expectedNFA)

    }

    @Test
    fun `toNFA  second test`() {

        val a: CharAlphabet = setOf<Letter>('a', 'b')
        val transitionsTable: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 0
            )
        )
        val stateCount: Int = transitionsTable.count()
        val startState: Int = 0
        val terminalStates: Set<Int> = setOf(0)

        val set0: Set<Int> = setOf(0)
        val transitionsTableNFA: List<Map<Char, Set<Int>>> = listOf(
            mapOf(
                'a' to set0,
                'b' to set0
            )
        )
        val stateCountNFA: Int = transitionsTableNFA.count()
        val startStateNFA: Int = 0
        val terminalStatesNFA: Set<Int> = setOf(0)

        val myDFA = FiniteAutomaton.DFA(
            stateCount, a, transitionsTable,
            startState, terminalStates)
        val auNFA = myDFA.toNFA()

        val expectedNFA = FiniteAutomaton.NFA(
            stateCountNFA, transitionsTableNFA,
            startStateNFA, terminalStatesNFA)

        assertEquals(auNFA, expectedNFA)
    }
    
    @Test
    //тест для автоматов, которые подходят под используемый метод конкатенации
    fun `concatenate DFA  first test`() {
        val a: CharAlphabet = setOf<Letter>('a', 'b')
        val transitionsTableFisrt: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 0,
                'b' to 1
            )
        )
        val transitionsTableSecond: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 0,
                'b' to 2
            ),
            mapOf(
                'a' to 2,
                'b' to 1
            )
        )
        val transitionsTableNFA: List<Map<Char, Set<Int>>> = listOf(
            mapOf(
                'a' to setOf(0),
                'b' to setOf(1)
            ),
            mapOf(
                'a' to setOf(0,3),
                'b' to setOf(1,3)
            ),
            mapOf(
                'a' to setOf(2),
                'b' to setOf(3)
            ),
            mapOf(
                'a' to setOf(2),
                'b' to setOf(4)
            ),
            mapOf(
                'a' to setOf(4),
                'b' to setOf(3)
            )
        )
        val transitionsTableExpected: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 1
            ),
            mapOf(
                'a' to 2,
                'b' to 3
            ),
            mapOf(
                'a' to 6,
                'b' to 4
            ),
            mapOf(
                'a' to 5,
                'b' to 7
            ),
            mapOf(
                'a' to 8,
                'b' to 3
            ),
            mapOf(
                'a' to 6,
                'b' to 7
            ),
            mapOf(
                'a' to 6,
                'b' to 3
            ),
            mapOf(
                'a' to 10,
                'b' to 7
            ),
            mapOf(
                'a' to 9,
                'b' to 7
            ),
            mapOf(
                'a' to 9,
                'b' to 3
            ),
            mapOf(
                'a' to 9,
                'b' to 7
            )
        )
        val startStateExpected: Int = 0
        val terminalStatesExpected: Set<Int> = setOf(4, 7, 8, 9, 10)
        val aExpected: CharAlphabet = setOf<Letter>('a', 'b')
        val startState: Int = 0
        val terminalStatesFirst: Set<Int> = setOf(1)
        val terminalStatesSecond: Set<Int> = setOf(2)
        val startStateNFA: Int = 0
        val terminalStatesNFA: Set<Int> = setOf(4)
        val aNFA: CharAlphabet = setOf<Letter>('a', 'b')

        val oneDFA = FiniteAutomaton.DFA(transitionsTableFisrt.size, a, 
        transitionsTableFisrt, 0, terminalStatesFirst)
        val secondDFA = FiniteAutomaton.DFA(transitionsTableSecond.size, a, 
        transitionsTableSecond, 0, terminalStatesSecond)
        val expectedDFA = FiniteAutomaton.DFA(transitionsTableExpected.size, a, 
        transitionsTableExpected, 0, terminalStatesExpected)
        val myNFA = FiniteAutomaton.NFA(transitionsTableNFA.size, 
        transitionsTableNFA, 0, terminalStatesNFA)


        mockkObject(myNFA)
        every { myNFA.toDFA() } returns expectedDFA
        mockkConstructor(FiniteAutomaton.NFA::class)
        every { FiniteAutomaton.NFA(transitionsTableNFA.size, transitionsTableNFA,
            0, terminalStatesNFA).toDFA() } returns expectedDFA
        val thirdDFA = oneDFA.concatenate(secondDFA)
        assertEquals(expectedDFA, thirdDFA)

    }
    
    @Test
    //тест для автоматов, которые не подходят под используемый метод конкатенации (1-1, 2-1)
    fun `concatenate DFA  second test`()
    {
        val a: CharAlphabet = setOf<Letter>('a', 'b')
        //ДКА автомат с одной вершиной
        val transitionsTableDFA: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 0,
                'b' to 0
            )
        )
        // ДКА автомат с двумя вершинами
        val transitionsTableDFA1: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 1,
                'b' to 0
            ),
            mapOf(
                'a' to 1,
                'b' to 0
            )
        )
        //ДКА две + одна
        val transitionsTableDFA2: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 1,
                'b' to 0
            ),
            mapOf(
                'a' to 2,
                'b' to 3
            ),
            mapOf(
                'a' to 2,
                'b' to 3
            ),
            mapOf(
                'a' to 2,
                'b' to 3
            )
        )
        // ДКА одна+две
        val transitionsTableDFA3: List<Map<Char, Int>> = listOf(
            mapOf(
                'a' to 1,
                'b' to 1
            ),
            mapOf(
                'a' to 2,
                'b' to 1
            ),
            mapOf(
                'a' to 2,
                'b' to 1
            )
        )
        //НКА для 1+1
        val transitionsTableNFA1: List<Map<Char, Set<Int>>> = listOf(
            mapOf(
                'a' to setOf(0),
                'b' to setOf(0),
                LAMBDA to setOf(1)
            ),
            mapOf(
                'a' to setOf(1),
                'b' to setOf(1)
            )
        )
        //НКА для а+б
        val transitionsTableNFA2: List<Map<Char, Set<Int>>> = listOf(
                mapOf(
                    'a' to setOf(0,2),
                    'b' to setOf(0,2)
                ),
                mapOf(
                    'a' to setOf(2),
                    'b' to setOf(1)
                ),
                mapOf(
                    'a' to setOf(2),
                    'b' to setOf(1)
                )
        )
        //НКА для б+a
        val transitionsTableNFA3: List<Map<Char, Set<Int>>> = listOf(
            mapOf(
                'a' to setOf(1),
                'b' to setOf(0)
            ),
            mapOf(
                'a' to setOf(1),
                'b' to setOf(0),
                LAMBDA to setOf(2)
            ),
            mapOf(
                'a' to setOf(2),
                'b' to setOf(2)
            )
        )
        val stateCount: Int = transitionsTableDFA.count()
        val startState: Int = 0
        val set0: Set<Int> = setOf(0)
        val transitionsTableNFA: List<Map<Char, Set<Int>>> = listOf(
            mapOf(
                'a' to set0,
                'b' to set0
            )
        )

        val stateCountNFA: Int = transitionsTableNFA.count()
        val startStateNFA: Int = 0
        val terminalStatesDFA0: Set<Int> = setOf(0)
        val terminalStatesDFA1: Set<Int> = setOf(1)

        //ДКА с одной вершиной (а)
        val aDFA = FiniteAutomaton.DFA(stateCount, a, transitionsTableDFA,
            startState, terminalStatesDFA0)
        //ДКА с двумя вершинами (б)
        val bDFA = FiniteAutomaton.DFA(transitionsTableDFA1.size, a, transitionsTableDFA1,
            startState, terminalStatesDFA1)
        //ДКА результат б+а
        val cDFA = FiniteAutomaton.DFA(transitionsTableDFA2.size, a, transitionsTableDFA2,
            startState, setOf(2))
        //ДКА результат а+б
        val dDFA = FiniteAutomaton.DFA(transitionsTableDFA3.size, a, transitionsTableDFA3,
            startState, setOf(2))
        //НФА 1+1
        val aNFA = FiniteAutomaton.NFA(transitionsTableNFA1.size, transitionsTableNFA1,
            startState, setOf(1))
        //НФА а+б
        val bNFA = FiniteAutomaton.NFA(transitionsTableNFA2.size, transitionsTableNFA2,
            startState, setOf(2))
        //НФА б+a
        val cNFA = FiniteAutomaton.NFA(transitionsTableNFA3.size, transitionsTableNFA3,
            startState, setOf(2))


        mockkConstructor(FiniteAutomaton.NFA::class)
        every { FiniteAutomaton.NFA(transitionsTableNFA1.size, transitionsTableNFA1,
            0, setOf(1)).toDFA() } returns aDFA
        val resDFA1 = aDFA.concatenate(aDFA) //а+а

        mockkConstructor(FiniteAutomaton.NFA::class)
        every { FiniteAutomaton.NFA(transitionsTableNFA2.size, transitionsTableNFA2,
            0, setOf(2)).toDFA() } returns bDFA
        val resDFA2 = aDFA.concatenate(bDFA) //а+б

        mockkConstructor(FiniteAutomaton.NFA::class)
        every { FiniteAutomaton.NFA(transitionsTableNFA3.size, transitionsTableNFA3,
            0, setOf(2)).toDFA() } returns cDFA
        val resDFA3 = bDFA.concatenate(aDFA) //б+а

        assertEquals(aDFA, resDFA1)
        assertEquals(bDFA, resDFA2)
        assertEquals(cDFA, resDFA3)
    }


    //endregion dfa-intersection tests


    //region aho-corasick tests
    @Test
    fun `Aho-Corasick with lowercase letters test`() {
        val patterns = listOf(
            "abc",
            "bcdc",
            "bcdd",
            "cccb",
            "bbbc"
        )

        val text = "abcdcbcddbbbcccbbbcccbb"
        val actual = ahoCorasick(patterns, text)

        val expected = mapOf(
            1 to listOf("abc"),
            2 to listOf("bcdc"),
            6 to listOf("bcdd"),
            10 to listOf("bbbc"),
            13 to listOf("cccb"),
            16 to listOf("bbbc"),
            19 to listOf("cccb")
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `Aho-Corasick with multiple pattetns in one index test`() {
        val patterns = listOf(
            "A",
            "AB",
            "ABC",
            "BC",
            "C",
            "CBA"
        )

        val text = "ABCBA"
        val actual = ahoCorasick(patterns, text)

        val expected = mapOf(
            1 to listOf("A", "AB", "ABC"),
            2 to listOf("BC"),
            3 to listOf("C", "CBA"),
            5 to listOf("A")
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `Aho-Corasick only one character test`() {
        val patterns = listOf(
            "a",
            "aa",
            "aaa",
            "aaaa",
            " # %%"
        )

        val text = "aaaaaaa"
        val actual = ahoCorasick(patterns, text)

        val expected = mapOf(
            1 to listOf("a", "aa", "aaa", "aaaa"),
            2 to listOf("a", "aa", "aaa", "aaaa"),
            3 to listOf("a", "aa", "aaa", "aaaa"),
            4 to listOf("a", "aa", "aaa", "aaaa"),
            5 to listOf("a", "aa", "aaa"),
            6 to listOf("a", "aa"),
            7 to listOf("a")
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `Aho-Corasick almost the same patterns test`() {
        val patterns = listOf(
            "ccd",
            "ccc",
            "ccm",
            "ccn"
        )

        val text = "cccdccnccmcccd"
        val actual = ahoCorasick(patterns, text)

        val expected = mapOf(
            1 to listOf("ccc"),
            2 to listOf("ccd"),
            5 to listOf("ccn"),
            8 to listOf("ccm"),
            11 to listOf("ccc"),
            12 to listOf("ccd")
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `Aho-Corasick with empty fields test`() {
        val actual = ahoCorasick(listOf(), "")
        val expected = mapOf<Int, List<String>>()

        assertEquals(expected, actual)
    }

    //endregion aho-corasick tests
}