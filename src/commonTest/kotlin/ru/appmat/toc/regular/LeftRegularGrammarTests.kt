package ru.appmat.toc.regular


import kotlin.test.*

typealias LeftRegularGrammar = RegularGrammar.LeftRegularGrammar
typealias LeftRule = RegularGrammarRule.NonTerminalRule.LeftRule
typealias LetterRule = RegularGrammarRule.LetterRule
typealias EmptyRule = RegularGrammarRule.EmptyRule
class LeftRegularGrammarTests {
    @Test
    fun renameNonTerminalsTests() {
        var grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('b'),
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                )
            )
        )

        var grammarS = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('S', 1u))
                ),
                NonTerminalSymbol('S', 1u) to setOf(
                    LetterRule('b'),
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('S', 1u))
                )
            )
        )

        assertEquals(grammarS,grammarA.renameNonTerminals('S'))

        grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u)),
                    LeftRule('d', NonTerminalSymbol('D', 0u)),
                    EmptyRule
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('c'),
                    LetterRule('d'),
                    LeftRule('d', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('D', 0u) to setOf(
                    LeftRule('d', NonTerminalSymbol('D', 0u)),
                    EmptyRule
                )
            )
        )

        grammarS = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('S', 1u)),
                    LeftRule('d', NonTerminalSymbol('S', 2u)),
                    EmptyRule
                ),
                NonTerminalSymbol('S', 1u) to setOf(
                    LetterRule('c'),
                    LetterRule('d'),
                    LeftRule('d', NonTerminalSymbol('S', 1u))
                ),
                NonTerminalSymbol('S', 2u) to setOf(
                    LeftRule('d', NonTerminalSymbol('S', 2u)),
                    EmptyRule
                )
            )
        )

        assertEquals(grammarS,grammarA.renameNonTerminals('S'))
    }

    @Test
    fun `concatenation of LeftRegularGrammars without LAMBDA in language`() {
        var grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('a', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('b'),
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                )
            )
        )

        var grammarB = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('A', 0u))
                ),
                NonTerminalSymbol('A', 0u) to setOf(
                    LeftRule('d', NonTerminalSymbol('A', 0u)),
                    LetterRule('d')
                )
            )
        )

        var grammarAB = LeftRegularGrammar(
            NonTerminalSymbol('A', 0u),
            mapOf(
                NonTerminalSymbol('A', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('A', 0u)),
                    LeftRule('b', NonTerminalSymbol('A', 1u))
                ),
                NonTerminalSymbol('A', 1u) to setOf(
                    LeftRule('d', NonTerminalSymbol('A', 1u)),
                    LeftRule('d', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('B', 0u)),
                    LeftRule('a', NonTerminalSymbol('B', 1u))
                ),
                NonTerminalSymbol('B', 1u) to setOf(
                    LetterRule('b'),
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('B', 1u))
                )
            )
        )

        assertEquals(grammarAB, grammarA.concatenate(grammarB))
    }

    @Test
    fun `concatenation of LeftRegularGrammars with LAMBDA in language`() {
        val grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('c'),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                )
            )
        )

        val grammarB = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('n', NonTerminalSymbol('N', 0u)),
                    EmptyRule
                ),
                NonTerminalSymbol('N', 0u) to setOf(
                    LeftRule('n', NonTerminalSymbol('N', 0u)),
                    LetterRule('k')
                )
            )
        )

        val grammarAB = LeftRegularGrammar(
            NonTerminalSymbol('A', 0u),
            mapOf(
                NonTerminalSymbol('A', 0u) to setOf(
                    LeftRule('n', NonTerminalSymbol('A', 1u)),
                    EmptyRule
                ),
                NonTerminalSymbol('A', 1u) to setOf(
                    LeftRule('n', NonTerminalSymbol('A', 1u)),
                    LeftRule('k', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('B', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 1u))
                ),
                NonTerminalSymbol('B', 1u) to setOf(
                    LeftRule('b', NonTerminalSymbol('B', 1u)),
                    LetterRule('c')
                )
            )
        )

        assertEquals(grammarAB, grammarA.concatenate(grammarB))
    }

    @Test
    fun starClosure(){
        var grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('b', NonTerminalSymbol('B', 0u)),
                    LeftRule('a', NonTerminalSymbol('S', 0u))
                ),
                NonTerminalSymbol('A', 0u) to setOf(
                    LetterRule('a'),
                    EmptyRule,
                    LeftRule('a', NonTerminalSymbol('A', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('c'),
                    EmptyRule,
                    LeftRule('b', NonTerminalSymbol('S', 0u))
                )
            )
        )
        var grammarB = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    EmptyRule,
                    LetterRule('b'),
                    LeftRule('b', NonTerminalSymbol('S', 0u)),
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('A', 0u) to setOf(
                    LetterRule('a'),
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('a', NonTerminalSymbol('A', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('c'),
                    LeftRule('c', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('S', 0u))
                )
            )
        )
        var grammarC = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    EmptyRule,
                    LetterRule('a'),
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('a', NonTerminalSymbol('A', 0u))
                ),
                NonTerminalSymbol('A', 0u) to setOf(
                    LetterRule('a'),
                    LeftRule('a', NonTerminalSymbol('S', 0u))
                )
            )
        )
        var grammarD = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LetterRule('a'),
                    LeftRule('a', NonTerminalSymbol('A', 0u))
                ),
                NonTerminalSymbol('A', 0u) to setOf(
                    LetterRule('a')
                )
            )
        )
        assertEquals(grammarB, grammarA.starClosure())
        assertEquals(grammarC, grammarD.starClosure())
    }

    @Test
    fun plusClosure(){
        var grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('b', NonTerminalSymbol('B', 0u)),
                    LeftRule('a', NonTerminalSymbol('S', 0u))
                ),
                NonTerminalSymbol('A', 0u) to setOf(
                    LetterRule('a'),
                    EmptyRule,
                    LeftRule('a', NonTerminalSymbol('A', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('c'),
                    EmptyRule,
                    LeftRule('b', NonTerminalSymbol('S', 0u))
                )
            )
        )
        var grammarB = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(

                    LetterRule('b'),
                    LeftRule('b', NonTerminalSymbol('S', 0u)),
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u))
                ),
                NonTerminalSymbol('A', 0u) to setOf(
                    LetterRule('a'),
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('a', NonTerminalSymbol('A', 0u))
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LetterRule('c'),
                    LeftRule('c', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('S', 0u))
                )
            )
        )
        var grammarC = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    EmptyRule,
                    LetterRule('a'),
                    LeftRule('a', NonTerminalSymbol('S', 0u))
                )

            )
        )
        assertEquals(grammarB, grammarA.plusClosure())
        assertEquals(grammarC, grammarC.plusClosure())
    }

        @Test
    fun `union LeftRegularGrammar with another containing only LetterRules`() {
        val grammarA = LeftRegularGrammar(
            NonTerminalSymbol('T', 0u),
            mapOf(
                NonTerminalSymbol('T', 0u) to setOf(
                    LeftRule('t', NonTerminalSymbol('T', 0u)),
                    LetterRule('t')
                )
            )
        )

        val grammarB = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LetterRule('a'),
                    LetterRule('f'),
                    LetterRule('c'),
                    LetterRule('e'),
                    LetterRule('g')
                )
            )
        )

        assertEquals(
            LeftRegularGrammar(
                NonTerminalSymbol('S', 0u),
                mapOf(
                    NonTerminalSymbol('S', 0u) to setOf(
                        LeftRule('t', NonTerminalSymbol('A', 0u)),
                        LetterRule('t'),
                        LetterRule('a'),
                        LetterRule('f'),
                        LetterRule('c'),
                        LetterRule('e'),
                        LetterRule('g')
                    ),
                    NonTerminalSymbol('A', 0u) to setOf(
                        LeftRule('t', NonTerminalSymbol('A', 0u)),
                        LetterRule('t')
                    ),
                    NonTerminalSymbol('B', 0u) to setOf(
                        LetterRule('a'),
                        LetterRule('f'),
                        LetterRule('c'),
                        LetterRule('e'),
                        LetterRule('g')
                    )
                )
            ),
            grammarA union grammarB
        )
    }

    @Test
    fun `union LeftRegularGrammar with another`() {
        val grammarA = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('a', NonTerminalSymbol('S', 0u)),
                    LeftRule('b', NonTerminalSymbol('B', 0u)),
                    LeftRule('c', NonTerminalSymbol('C', 0u)),
                    LetterRule('l')
                ),
                NonTerminalSymbol('B', 0u) to setOf(
                    LeftRule('b', NonTerminalSymbol('B', 0u)),
                    LetterRule('b')
                ),
                NonTerminalSymbol('C', 0u) to setOf(
                    LeftRule('c', NonTerminalSymbol('C', 0u)),
                    LetterRule('c')
                )
            )
        )

        val grammarB = LeftRegularGrammar(
            NonTerminalSymbol('S', 0u),
            mapOf(
                NonTerminalSymbol('S', 0u) to setOf(
                    LeftRule('e', NonTerminalSymbol('S', 0u)),
                    LeftRule('t', NonTerminalSymbol('T', 0u)),
                    LetterRule('c')
                ),
                NonTerminalSymbol('T', 0u) to setOf(
                    LeftRule('t', NonTerminalSymbol('T', 0u)),
                    LetterRule('d'),
                    LetterRule('r'),
                    LetterRule('n')
                )
            )
        )

        assertEquals(
            LeftRegularGrammar(
                NonTerminalSymbol('S', 0u),
                mapOf(
                    NonTerminalSymbol('S', 0u) to setOf(
                        LeftRule('a', NonTerminalSymbol('A', 0u)),
                        LeftRule('b', NonTerminalSymbol('A', 1u)),
                        LeftRule('c', NonTerminalSymbol('A', 2u)),
                        LetterRule('l'),
                        LeftRule('e', NonTerminalSymbol('B', 0u)),
                        LeftRule('t', NonTerminalSymbol('B', 1u)),
                        LetterRule('c')
                    ),
                    NonTerminalSymbol('A', 0u) to setOf(
                        LeftRule('a', NonTerminalSymbol('A', 0u)),
                        LeftRule('b', NonTerminalSymbol('A', 1u)),
                        LeftRule('c', NonTerminalSymbol('A', 2u)),
                        LetterRule('l')
                    ),
                    NonTerminalSymbol('A', 1u) to setOf(
                        LeftRule('b', NonTerminalSymbol('A', 1u)),
                        LetterRule('b')
                    ),
                    NonTerminalSymbol('A', 2u) to setOf(
                        LeftRule('c', NonTerminalSymbol('A', 2u)),
                        LetterRule('c')
                    ),
                    NonTerminalSymbol('B', 0u) to setOf(
                        LeftRule('e', NonTerminalSymbol('B', 0u)),
                        LeftRule('t', NonTerminalSymbol('B', 1u)),
                        LetterRule('c')
                    ),
                    NonTerminalSymbol('B', 1u) to setOf(
                        LeftRule('t', NonTerminalSymbol('B', 1u)),
                        LetterRule('d'),
                        LetterRule('r'),
                        LetterRule('n')
                    )
                )
            ),
            grammarA union grammarB
        )
    }

    @Test
    fun `union LeftRegularGrammar with itself`() {
        val b = NonTerminalSymbol('B')
        val a = NonTerminalSymbol('A')

        val leftRegularGrammar = LeftRegularGrammar(
            a,
            mapOf(
                a to setOf(
                    LeftRule('b', b),
                    LetterRule('c')
                ),
                b to setOf(
                    LeftRule('a', a)
                )
            )
        )
        val s = NonTerminalSymbol('S')

        assertEquals(
            LeftRegularGrammar(
                s,
                mapOf(
                    s to setOf(
                        LeftRule('b', a.copy(index = 1u)),
                        LeftRule('b', b.copy(index = 1u)),
                        LetterRule('c')
                    ),
                    a to setOf(
                        LeftRule('b', a.copy(index = 1u)),
                        LetterRule('c')
                    ),
                    b to setOf(
                        LeftRule('b', b.copy(index = 1u)),
                        LetterRule('c')
                    ),
                    a.copy(index = 1u) to setOf(
                        LeftRule('a', a)
                    ),
                    b.copy(index = 1u) to setOf(
                        LeftRule('a', b)
                    )
                )
            ),
            leftRegularGrammar union leftRegularGrammar
        )
    }
    
    @Test
    fun `toLambdaFree Common Case`(){
        val s = NonTerminalSymbol('S')
        val a = NonTerminalSymbol('A')
        val b = NonTerminalSymbol('B')
        val c = NonTerminalSymbol('C')

        val leftRegularGrammar = LeftRegularGrammar(
            s,
            mapOf(
                s to setOf(
                    LeftRule('b',a),
                    LeftRule('a',b),
                    LeftRule('b',c),
                    EmptyRule
                ),
                a to setOf(
                    LeftRule('a',s),
                    LeftRule('a',c),
                    EmptyRule
                ),
                b to setOf(
                    LetterRule('a'),
                    EmptyRule
                ),
                c to setOf(
                    EmptyRule
                )
            )
        )

        assertEquals(
            LeftRegularGrammar(
                s,
                mapOf(
                    s to setOf(
                        LeftRule('b',a),
                        LeftRule('a',b),
                        EmptyRule,
                        LetterRule('b'),
                        LetterRule('a')
                    ),
                    a to setOf(
                        LeftRule('a',s),
                        LetterRule('a')
                    ),
                    b to setOf(
                        LetterRule('a')
                    )
                )
            ), leftRegularGrammar.toLambdaFree()
        )
    }
}
