package ru.appmat.toc.regular

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

typealias Lambda = RegularExpression.Component.Lambda
typealias LetterComponent = RegularExpression.Component.Letter
typealias Star = RegularExpression.Component.Quantifier.Star
typealias Plus = RegularExpression.Component.Quantifier.Plus
typealias Optional = RegularExpression.Component.Quantifier.Optional
typealias Alternation = RegularExpression.Component.NAryOperator.Alternation
typealias Concatenation = RegularExpression.Component.NAryOperator.Concatenation


class RegularExpressionTests {
    @Test
    fun stringTest() {
        assertEquals("$LAMBDA", RegularExpression.of("$LAMBDA").toString())
        assertEquals("a|b+", RegularExpression.of("a|b+").toString())
        assertEquals("ab*c+", RegularExpression.of("ab*c+").toString())
        assertEquals("ab*c+", RegularExpression.of("a(b)*c+").toString())
        assertEquals("(ab*c)+", RegularExpression.of("(a(b)*c)+").toString())
        assertEquals("ab*((c|d)e?)+", RegularExpression.of("ab*((c|d)e?)+").toString())
        assertEquals("(ab*c)+cd(a?b)?", RegularExpression.of("(ab*c)+cd(a?b)?").toString())
        assertEquals("a*(ab*cd+)+", RegularExpression.of("a*(a(b*c)d+)+").toString())
        assertEquals("ag|eaaaaac|daf|t", RegularExpression.of("ag|eaaaaac|daf|t").toString())
        assertEquals(
            "a(b?(c*d|f|g)?t|a?c*)*d|r",
            RegularExpression.of("a(b?(c*d|(f|g))?t|(a?c*))*d|r").toString()
        )
        assertEquals(
            "a*bc|de*(f+((de?)+cdf?e|bcd?(df)?f?)+bc|de|f|ef)?(de|f)a+b|ce?",
            RegularExpression.of("a*bc|de*(f+((de?)+((cd)f?e)|(bcd?)(df)?f?)+bc|de|(f|ef))?(de|f)a+b|ce?").toString()
        )
    }

    @Test
    fun structureTest() {
        assertEquals(RegularExpression(Lambda), RegularExpression.of(""))
        assertEquals(RegularExpression(Lambda), RegularExpression.of("$LAMBDA"))


        assertEquals(
            RegularExpression(
                Concatenation(
                    listOf(
                        LetterComponent('a'),
                        LetterComponent('b')
                    )
                )
            ),
            RegularExpression.of("ab")
        )

        assertEquals(
            RegularExpression(
                Concatenation(
                    listOf(
                        LetterComponent('a'),
                        LetterComponent('b'),
                        Optional(
                            Plus(
                                LetterComponent('c')
                            )
                        )
                    )
                )
            ), RegularExpression.of("abc+?")
        )

        assertEquals(
            RegularExpression(
                Alternation(
                    listOf(
                        LetterComponent('a'),
                        LetterComponent('b'),
                        LetterComponent('c')
                    )
                )
            ), RegularExpression.of("a|b|c")
        )

        assertEquals(
            RegularExpression(
                Alternation(
                    listOf(
                        LetterComponent('a'),
                        LetterComponent('b'),
                        LetterComponent('c'),
                        LetterComponent('d')
                    )
                )
            ), RegularExpression.of("a|(b|(c|d))")
        )

        assertEquals(
            RegularExpression(
                Concatenation(
                    listOf(
                        LetterComponent('a'),
                        Alternation(
                            listOf(
                                LetterComponent('b'),
                                LetterComponent('c'),
                                LetterComponent('d')
                            )
                        )
                    )
                )
            ), RegularExpression.of("a(b|c|d)")
        )


        assertEquals(
            RegularExpression(
                Alternation(
                    listOf(
                        LetterComponent('a'),
                        Optional(
                            LetterComponent('b')
                        )
                    )
                )
            ), RegularExpression.of("a|b?")
        )

        assertEquals(RegularExpression(Lambda), RegularExpression.of("()"))
    }

    @Test
    fun illegalArgumentTest() {
        assertFailsWith<IllegalArgumentException> { RegularExpression.of(")(ab+") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("+") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("|") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("a(?b+)") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("(((ab+)") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("a(|bc+)") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("a(bc+|)") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("abc+)") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("abc)+") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("ab||c)+") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("ab+||c)+") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("ab(cd(|)ed)") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("ab+(|cde)+") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("|abc)+") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("abc+|") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("abc+|+") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("a(b*(d+c(+)))") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("(|)") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("*ab") }
        assertFailsWith<IllegalArgumentException> { RegularExpression.of("*+ab") }
    }

    @Test
    fun regexBasicTest() {
        var a = RegularExpression.of("$LAMBDA")
        var b = RegularExpression.of("")
        assertEquals(RegularExpression(Lambda), a.concatenate(b))
        assertEquals(RegularExpression(Lambda), b.concatenate(a))
        assertEquals(RegularExpression(Lambda), a.union(b))
        assertEquals(RegularExpression(Lambda), b.union(a))
        assertEquals(RegularExpression(Lambda), a.plusClosure())
        assertEquals(RegularExpression(Lambda), a.starClosure())

        a = RegularExpression.of("a|bc+d")
        b = RegularExpression.of("e(fg)?")
        assertEquals("a|bc+d|e(fg)?", a.union(b).toString())
        assertEquals("e(fg)?|a|bc+d", b.union(a).toString())
        assertEquals("a|bc+de(fg)?", a.concatenate(b).toString())
        assertEquals("e(fg)?a|bc+d", b.concatenate(a).toString())
        assertEquals("(a|bc+d)+", a.plusClosure().toString())
        assertEquals("(e(fg)?)*", b.starClosure().toString())

        a = RegularExpression.of("a*(bab|c)?")
        b = RegularExpression.of("(d|e+(be)?)+")
        assertEquals("a*(bab|c)?|(d|e+(be)?)+", a.union(b).toString())
        assertEquals("(d|e+(be)?)+|a*(bab|c)?", b.union(a).toString())
        assertEquals("a*(bab|c)?(d|e+(be)?)+", a.concatenate(b).toString())
        assertEquals("(d|e+(be)?)+a*(bab|c)?", b.concatenate(a).toString())
        assertEquals("(a*(bab|c)?)+", a.plusClosure().toString())
        assertEquals("(d|e+(be)?)+*", b.starClosure().toString())

        a = RegularExpression.of("")
        b = RegularExpression.of("a*(b+|c)+")
        assertEquals("a*(b+|c)+", a.concatenate(b).toString())
        assertEquals("a*(b+|c)+", b.concatenate(a).toString())
        assertEquals("$LAMBDA|a*(b+|c)+", a.union(b).toString())
        assertEquals("a*(b+|c)+|$LAMBDA", b.union(a).toString())
        assertEquals("(a*(b+|c)+)+", b.plusClosure().toString())
        assertEquals("$LAMBDA", a.starClosure().toString())

        a = RegularExpression.of("a|b")
        b = RegularExpression.of("a|b|d+|e?")
        assertEquals("a|ba|b|d+|e?", a.concatenate(b).toString())
        assertEquals("a|b|d+|e?a|b", b.concatenate(a).toString())
        assertEquals("a|b|a|b|d+|e?", a.union(b).toString())
        assertEquals("a|b|d+|e?|a|b", b.union(a).toString())
        assertEquals("(a|b)+", a.plusClosure().toString())
        assertEquals("(a|b)*", a.starClosure().toString())
    }
}
