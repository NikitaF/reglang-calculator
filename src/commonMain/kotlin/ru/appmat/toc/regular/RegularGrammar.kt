package ru.appmat.toc.regular

enum class Orientation {
    Right, Left
}

data class NonTerminalSymbol(val letter: Char, val index: UInt = 0u) {
    override fun toString(): String {
        return "${letter.toUpperCase()}$index"
    }
}

sealed class RegularGrammarRule {
    object EmptyRule : RegularGrammarRule() {
        override fun toString(): String = LAMBDA.toString()
    }

    data class LetterRule(val letter: Char) : RegularGrammarRule() {
        override fun toString() = "$letter"
    }

    sealed class NonTerminalRule(val orientation: Orientation) : RegularGrammarRule() {

        data class RightRule(val letter: Char, val nonTerminal: NonTerminalSymbol) :
            NonTerminalRule(Orientation.Right) {
            override fun toString(): String = "$letter$nonTerminal"
        }

        data class LeftRule(val letter: Char, val nonTerminal: NonTerminalSymbol) :
            NonTerminalRule(Orientation.Left) {
            override fun toString(): String = "$nonTerminal$letter"
        }
    }
}


sealed class RegularGrammar<T : Regular<T>>(
    open val startVariable: NonTerminalSymbol,
    open val rules: Map<NonTerminalSymbol, Set<RegularGrammarRule>>,
    open val orientation: Orientation
) : Regular<T> {

    fun isValid() = rules.values.flatten().all {
        when (it) {
            is RegularGrammarRule.NonTerminalRule -> it.orientation == orientation
            else -> true
        }
    }

    abstract fun toLambdaFree(): T

    abstract fun withRule(rule: RegularGrammarRule): T

    abstract fun renameNonTerminals(letter: Letter): T

    data class LeftRegularGrammar(
        override val startVariable: NonTerminalSymbol,
        override val rules: Map<NonTerminalSymbol, Set<RegularGrammarRule>>
    ) : RegularGrammar<LeftRegularGrammar>(startVariable, rules, Orientation.Left) {

        private val producesEmptyWord = this.rules[this.startVariable]!!.contains(RegularGrammarRule.EmptyRule)

        init {
            require(isValid())
        }

        /**
         * Задача:
         * звезда Клини
         * Исполнитель: [A-05-17] Попов Владимир
         */
        //region left-grammar-closure
        override fun starClosure(): LeftRegularGrammar {
            val newGrammar = this.plusClosure()
            val changed = newGrammar.rules.mapValues { it.value.toMutableSet() }.toMutableMap()
            changed[newGrammar.startVariable]!!.add(RegularGrammarRule.EmptyRule)
            return LeftRegularGrammar(newGrammar.startVariable, changed)
        }

        override fun plusClosure(): LeftRegularGrammar {

            val newGrammar = this.toLambdaFree()
            val changed = newGrammar.rules.mapValues { (_, states) ->
                states.flatMap { rule ->
                    if (rule is RegularGrammarRule.LetterRule)
                        listOf(rule, RegularGrammarRule.NonTerminalRule.LeftRule(rule.letter, newGrammar.startVariable))
                    else
                        listOf(rule)
                }.toSet()
            }
            return LeftRegularGrammar(this.startVariable, changed)
        }

        //endregion


        /**
         * Задача:
         * конкатенация
         * Исполнитель [А-05-17] Окованцев Павел
         */
        override fun concatenate(other: LeftRegularGrammar): LeftRegularGrammar {
            val renamedOther = other.toLambdaFree().renameNonTerminals('A')
            val renamedThis = this.renameNonTerminals('B')

            val resRules =
                renamedOther.rules.map { (nonTerminal, ruleSet) ->
                    nonTerminal to ruleSet.map { rule ->
                        when (rule) {
                            is RegularGrammarRule.LetterRule -> RegularGrammarRule.NonTerminalRule.LeftRule(
                                rule.letter,
                                renamedThis.startVariable
                            )
                            else -> rule
                        }
                    }.toSet()
                }.union(renamedThis.rules.toList()).toMap()

            return LeftRegularGrammar(renamedOther.startVariable, resRules)
        }

        override fun renameNonTerminals(letter: Letter): LeftRegularGrammar {
            // old nonTerminalSymbol -> new nonTerminalSymbol
            val nonTerminalsMap =
                this.rules.keys.mapIndexed { index, old -> Pair(old, NonTerminalSymbol(letter, index.toUInt())) }
                    .toMap()

            val newRules = this.rules.map { (nonTerminal, ruleSet) ->
                nonTerminalsMap[nonTerminal]!! to ruleSet.map { rule ->
                    if (rule is RegularGrammarRule.NonTerminalRule.LeftRule)
                        rule.copy(nonTerminal = nonTerminalsMap[rule.nonTerminal]!!)
                    else rule
                }.toSet()
            }.toMap()

            return LeftRegularGrammar(nonTerminalsMap[this.startVariable]!!, newRules)
        }

        override fun intersect(other: LeftRegularGrammar): LeftRegularGrammar {
            TODO("Not yet implemented")
        }


        override fun complement(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * объединение
         * Исполнитель [А-13-17] Бернадская Алина
         */
        override infix fun union(other: LeftRegularGrammar): LeftRegularGrammar {
            val renamedThis = this.renameNonTerminals('A')
            val renamedOther = other.renameNonTerminals('B')
            val startVariable = NonTerminalSymbol('S', 0u)

            val unionRules = mutableMapOf<NonTerminalSymbol, Set<RegularGrammarRule>>()
            unionRules[startVariable] = renamedThis.rules[renamedThis.startVariable]!!.union(
                renamedOther.rules[renamedOther.startVariable]!!
            )

            unionRules.putAll(renamedThis.rules)
            unionRules.putAll(renamedOther.rules)

            return LeftRegularGrammar(startVariable, unionRules)
        }

        /**
         * Задача:
         * грамматика для реверса языка
         */
        override fun reversed(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun withRule(rule: RegularGrammarRule): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * КЯК
         * Исполнитель: [А-05-17] Хижин Вадим
         */
        override fun check(word: Word): Boolean {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразование к НКА
         * исполнитель: [А-05-17] Лосев Дмитрий
         */
        fun toNFA(): FiniteAutomaton.NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразование к правой грамматике
         * Исполнитель: [А-05-17] Байков Юрий
         */
        fun toRight(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Избавиться от всех lambda, кроме S -> lambda
         * Исполнитель: [А-05-17] Васильева Анна
         */
        override fun toLambdaFree(): LeftRegularGrammar {
            
            val (onlyLambda, notOnlyLambda) =
                this.rules.entries.filterNot { it.key == startVariable }
                    .filter { RegularGrammarRule.EmptyRule in it.value }
                    .partition { it.value.size == 1 }

            val onlyLambdaNT = onlyLambda.map { it.key }
            val notOnlyLambdaNT = notOnlyLambda.map { it.key }

            var newRules = this.rules.map { (nonTerminal, ruleSet) ->
                nonTerminal to ruleSet.flatMap { rule ->
                    if (rule is RegularGrammarRule.NonTerminalRule.LeftRule)
                        when (rule.nonTerminal) {
                            in onlyLambdaNT -> listOf(RegularGrammarRule.LetterRule(rule.letter))
                            in notOnlyLambdaNT -> listOf(RegularGrammarRule.LetterRule(rule.letter), rule)
                            else -> listOf(rule)
                        }
                    else listOf(rule)
                }.toSet()
            }.toMap()


            newRules = newRules.filterNot { (nt, _) -> nt in onlyLambdaNT }
                .map { (nonTerminal, ruleSet) ->
                    if (nonTerminal == startVariable) nonTerminal to ruleSet
                    else nonTerminal to ruleSet.filterNot { rule -> rule == RegularGrammarRule.EmptyRule }.toSet()
                }.toMap()

            return LeftRegularGrammar(startVariable, newRules)
        }

    }

    data class RightRegularGrammar(
        override val startVariable: NonTerminalSymbol,
        override val rules: Map<NonTerminalSymbol, Set<RegularGrammarRule>>
    ) : RegularGrammar<RightRegularGrammar>(startVariable, rules, Orientation.Right) {

        private val producesEmptyWord = this.rules[this.startVariable]!!.contains(RegularGrammarRule.EmptyRule)

        init {
            require(isValid())
        }

        /**
         * Задача:
         * звезда Клини
         * Исполнитель: [А-05-17] Горшков Виктор
         */
        //region right-grammar-closure
        @ExperimentalUnsignedTypes
        override fun starClosure(): RightRegularGrammar {
            val newGrammar = this.plusClosure()
            return if (!newGrammar.producesEmptyWord) newGrammar.addEmptyWord() else newGrammar
        }

        override fun plusClosure(): RightRegularGrammar {
            return this.toLambdaFree().addLinksOnAxiom()
        }

        private fun addLinksOnAxiom(): RightRegularGrammar {
            val newRules = this.rules.mapValues {
                it.value.flatMap { rule ->
                    if (rule is RegularGrammarRule.LetterRule)
                        listOf(rule, RegularGrammarRule.NonTerminalRule.RightRule(rule.letter, this.startVariable))
                    else
                        listOf(rule)
                }.toSet()
            }
            return RightRegularGrammar(this.startVariable, newRules)
        }

        @ExperimentalUnsignedTypes
        private fun addEmptyWord(): RightRegularGrammar {
            val renamedGrammarRule = this.renameNonTerminals(letter = 'B')
            val startVar = renamedGrammarRule.startVariable
            val newRules = renamedGrammarRule.rules.toMutableMap()
            val newStartVar = NonTerminalSymbol(letter = 'B', index = newRules.size.toUInt())
            newRules[newStartVar] = newRules[startVar]!!.plus(RegularGrammarRule.EmptyRule).toSet()
            return RightRegularGrammar(newStartVar, newRules)
        }
        //endregion

        /**
         * Задача:
         * конкатенация
         * Исполнитель: [А-13-17] Екатерина Бондарь
         */
        override fun concatenate(other: RightRegularGrammar): RightRegularGrammar {
            val renamedThis = this.toLambdaFree().renameNonTerminals('A')
            val renamedOther = other.renameNonTerminals('B')

            val resRules =
                renamedThis.rules.map { (nonTerminal, ruleSet) ->
                    nonTerminal to ruleSet.map { rule ->
                        when (rule) {
                            is RegularGrammarRule.LetterRule -> RegularGrammarRule.NonTerminalRule.RightRule(
                                rule.letter,
                                renamedOther.startVariable
                            )
                            else -> rule
                        }
                    }.toSet()
                }.union(renamedOther.rules.toList()).toMap()

            return RightRegularGrammar(renamedThis.startVariable, resRules)
        }

        override fun intersect(other: RightRegularGrammar): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun complement(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * объединение
         * Исполнитель: [А-13-17] Алиева Ирада
         */
        override infix fun union(other: RightRegularGrammar): RightRegularGrammar {
            val renamedThis = this.renameNonTerminals('A')
            val renamedOther = other.renameNonTerminals('B')
            val startVariable = NonTerminalSymbol('S', 0u)

            val unionRules = mutableMapOf<NonTerminalSymbol, Set<RegularGrammarRule>>()
            unionRules[startVariable] = renamedThis.rules[renamedThis.startVariable]!!.union(
                renamedOther.rules[renamedOther.startVariable]!!
            )

            unionRules.putAll(renamedThis.rules)
            unionRules.putAll(renamedOther.rules)

            return RightRegularGrammar(startVariable, unionRules)
        }

        /**
         * Задача:
         * грамматика для реверса языка
         */
        override fun reversed(): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        override fun withRule(rule: RegularGrammarRule): RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * КЯК
         * Исполнитель: [A-13-17] Сидоров Кирилл
         */
        override fun check(word: Word): Boolean {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразование к НКА
         * Исполнитель: [A-05-17] Фирагин Никита
         */
        fun toNFA(): FiniteAutomaton.NFA {

            val transitionsTable: List<Map<Char, Set<Int>>> //Таблица переходов (таблица смежности)
            val terminalStates = mutableSetOf<Int>()        //Конечные состояния НКА
            val startIndex: Int = 0                         //Стартовая вершина
            val stateCount: Int                             //Кол-во состояний

            val nonTermToState = mutableMapOf<String,Int>() //Словарь нетерминальный символ - номер состояния НКА

            var count: Int = 0
            for (nonTerm in rules.keys){

                val nt = nonTerm.toString()
                nonTermToState[nt] = count
                count++
            }

            val tTable = mutableMapOf<Char,MutableSet<Int>>()
            val mtTransTable = mutableListOf<Map<Char, MutableSet<Int>>>()

            for (elem in rules.values)
                {
                    for (x in elem)
                    {
                       count = transTable(nonTermToState, x, tTable, count, terminalStates)
                    }
                    mtTransTable.add(HashMap(tTable))
                    tTable.clear()
            }

            transitionsTable = mtTransTable
            stateCount = transitionsTable.count() + terminalStates.count()

            return FiniteAutomaton.NFA(stateCount, transitionsTable, startIndex, terminalStates)

        }

        private fun transTable(nonTermToState: MutableMap<String, Int>, x: RegularGrammarRule,
         tTable: MutableMap<Char, MutableSet<Int>>, count: Int, terminalStates: MutableSet<Int>): Int {
            var count1 = count
            if (nonTermToState[x.toString().substring(1)] != null) {
                if (tTable[x.toString()[0]] != null)
                    tTable[x.toString()[0]]!!.add(nonTermToState[x.toString().substring(1)]!!)
                else
                    tTable[x.toString()[0]] = mutableSetOf(nonTermToState[x.toString().substring(1)]!!)
            } else {
                if (tTable[x.toString()[0]] != null)
                    tTable[x.toString()[0]]!!.add(count1)
                else
                    tTable[x.toString()[0]] = mutableSetOf(count1)

                terminalStates.add(count1)
                count1++
            }
            return count1
        }

        /**
         * Задача:
         * Преобразование к левой грамматике
         * Исполнитель: [A-05-17] Курбанов Амирбег
         */
        fun toLeft(): LeftRegularGrammar {
            TODO("Not yet implemented")
        }

        fun toRegExp(): RegularExpression = toNFA().toRegExp()

        fun toDFA(): FiniteAutomaton.DFA = toNFA().toDFA()

        /**
         * Задача:
         * Избавиться от всех lambda, кроме S -> lambda
         * Исполнитель: [A-05-17] Никишин Даниил
         */
        override fun toLambdaFree(): RightRegularGrammar {
            val (onlyLambda, notOnlyLambda) =
                this.rules.entries.filterNot { it.key == startVariable }
                    .filter { RegularGrammarRule.EmptyRule in it.value }
                    .partition { it.value.size == 1 }

            val onlyLambdaNT = onlyLambda.map { it.key }
            val notOnlyLambdaNT = notOnlyLambda.map { it.key }

            var newRules = this.rules.map { (nonTerminal, ruleSet) ->
                nonTerminal to ruleSet.flatMap { rule ->
                    if (rule is RegularGrammarRule.NonTerminalRule.RightRule)
                        when (rule.nonTerminal) {
                            in onlyLambdaNT -> listOf(RegularGrammarRule.LetterRule(rule.letter))
                            in notOnlyLambdaNT -> listOf(RegularGrammarRule.LetterRule(rule.letter), rule)
                            else -> listOf(rule)
                        }
                    else listOf(rule)
                }.toSet()
            }.toMap()

            newRules = newRules.filterNot { (nt, _) -> nt in onlyLambdaNT }
                .map { (nonTerminal, ruleSet) ->
                    if (nonTerminal == startVariable) nonTerminal to ruleSet
                    else nonTerminal to ruleSet.filterNot { rule -> rule == RegularGrammarRule.EmptyRule }.toSet()
                }.toMap()

            return RightRegularGrammar(startVariable, newRules)
        }

        override fun renameNonTerminals(letter: Letter): RightRegularGrammar {
            // old nonTerminalSymbol -> new nonTerminalSymbol
            val nonTerminalsMap =
                this.rules.keys.mapIndexed { index, old -> Pair(old, NonTerminalSymbol(letter, index.toUInt())) }
                    .toMap()

            val newRules = this.rules.map { (nonTerminal, ruleSet) ->
                nonTerminalsMap[nonTerminal]!! to ruleSet.map { rule ->
                    if (rule is RegularGrammarRule.NonTerminalRule.RightRule)
                        rule.copy(nonTerminal = nonTerminalsMap[rule.nonTerminal]!!)
                    else rule
                }.toSet()
            }.toMap()

            return RightRegularGrammar(nonTerminalsMap[this.startVariable]!!, newRules)
        }
    }

}
