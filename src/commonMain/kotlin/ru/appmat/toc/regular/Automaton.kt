package ru.appmat.toc.regular

typealias NondeterministicFiniteAutomaton = FiniteAutomaton.NFA
typealias DeterministicFiniteAutomaton = FiniteAutomaton.DFA

sealed class FiniteAutomaton<AutomatonType : FiniteAutomaton<AutomatonType>>(
    open val stateCount: Int
) : Regular<AutomatonType> {

    /**
     * Задача
     * Описать структуру данных ДКА и реализовать dfa-basic
     * Исполнитель: [А-13-17] Ракипов Динаф
     */
    data class DFA(
        override val stateCount: Int,                    //Кол-во состояний
        val alphabet: CharAlphabet,                      //Алфавит
        val transitionsTable: List<Map<Char, Int>>,      //Таблица переходов (таблица смежности)
        val startIndex: Int,                             //Стартовая вершина
        val terminalStates: Set<Int>                     //Терминальные вершины
    ) : FiniteAutomaton<DFA>(stateCount) {

        init {
            if (!(transitionsTable.all { alphabet == it.keys })) {
                throw IllegalArgumentException("Transitions table is not complete or has extra letters")
            }

            val allStates = (0 until stateCount).toSet();

            if (!(transitionsTable.all { it -> it.all { allStates.contains(it.value) } })) {
                throw IllegalArgumentException("Transitions table has transitions to a nonexistent state")
            }
        }

        //region dfa-basic
        override fun check(word: Word): Boolean {
            return check(word, startIndex)
        }

        private tailrec fun check(word: Word, currState: Int): Boolean {
            return when {
                word.isEmpty() -> terminalStates.contains(currState)
                alphabet.contains(word[0]) -> check(
                    word.substring(1, word.length),
                    transitionsTable[currState][word[0]]!!
                )
                else -> false;
            }
        }

        /**
         * Должна расширять алфавит текущего ДКА
         */
        fun withAlphabet(alphabet: CharAlphabet): DFA {

            if (!alphabet.containsAll(this.alphabet)) {
                throw IllegalArgumentException("Method param alphabet does not contain DFA alphabet")
            }

            if (alphabet == this.alphabet) {
                return DFA(stateCount, alphabet, transitionsTable, startIndex, terminalStates)
            }

            val diffAlphabet = alphabet.minus(this.alphabet)
            val newTransitionsTable = transitionsTable.map { it.toMutableMap() }.toMutableList()

            //Создаем тупиковую вершину
            val deadlockIndex: Int = newTransitionsTable.size
            val deadlockState = alphabet.map { it -> Pair<Letter, Int>(it, deadlockIndex) }.toMap().toMutableMap()
            newTransitionsTable.add(deadlockState)

            //Обновляем таблицу переходов
            newTransitionsTable.forEach { state ->
                diffAlphabet.forEach {
                    state[it] = deadlockIndex
                }
            }

            return DFA(stateCount + 1, alphabet, newTransitionsTable, startIndex, terminalStates);
        }

        /**
         * Complement - нетерминалы превращаем в терминалы и наоброт
         * */
        override fun complement(): DFA {
            val newTerminalStates = (0 until stateCount).toSet().minus(terminalStates)
            return DFA(stateCount, alphabet, transitionsTable, startIndex, newTerminalStates);
        }
        //endregion dfa-basic

        fun deleteUnreachableStates(): DFA {
            val reachableStates = getReachablePeaks()// получили вершины, в которые можем попасть
            val unreachableStates = (0 until stateCount).minus(reachableStates)//недоступные, "бесполезные" вершины
            val displacementMap = reachableStates.map { it -> it to 0 }.toMap().toMutableMap()
            //получаем map'у из пар старый номер вершины - новый номер вершины
            var i = 0
            for (j in 0 until stateCount) {
                displacementMap[j] = j - i
                if (unreachableStates.contains(j))
                    i++
            }

            val newTransitionsTable = transitionsTable.map { it.toMutableMap() }.toMutableList()
            unreachableStates.reversed().forEach { newTransitionsTable.removeAt(it) }

            //делаем смещения
            i = 0
            while (i != newTransitionsTable.size) {
                for ((key, value) in newTransitionsTable[i])
                    newTransitionsTable[i][key] = displacementMap[value]!!
                i++
            }

            //смещаем терминальные и стартовое состояния
            val newTerminalStates = terminalStates.minus(unreachableStates).map { displacementMap[it]!! }
            val newStartIndex = displacementMap[startIndex]!!

            return this.copy(
                stateCount = newTransitionsTable.size,
                transitionsTable = newTransitionsTable,
                startIndex = newStartIndex,
                terminalStates = newTerminalStates.toSet()
            )
        }


        private fun getReachablePeaks(): Set<Int> {
            val reached = mutableSetOf<Int>()
            val queue = mutableSetOf<Int>(startIndex)
            queue.add(startIndex)
            while (queue.isNotEmpty()) {
                queue.addAll(transitionsTable[queue.first()].values.minus(reached))
                reached.add(queue.first())
                queue.remove(queue.first())
            }
            return reached
        }

        /**
         * Задача:
         * звезда Клини
         *	Исполнитель: [А-05-17] Павлов Иван
         */
        //region dfa-closure
        override fun starClosure(): DFA {
            TODO("Not yet implemented")
        }

        override fun plusClosure(): DFA {
            TODO("Not yet implemented")
        }
        //endregion dfa-closure


        /**
         * Задача:
         * конкатенация и преобразование в НКА
         * Исполнитель: [А-05-17] Кружкова Мария
         */
        //region dfa-concatenation
        override fun concatenate(other: DFA): DFA {
            val thisNFA = this.toNFA()
            val otherNFA = other.toNFA()

            val newTransitionsTable = thisNFA.transitionsTable.map {
                it.mapValues { (_, state) -> state.toMutableSet() }.toMutableMap()
            }.toMutableList()

            newTransitionsTable.forEachIndexed { index, transition ->
                if (thisNFA.terminalStates.contains(index)) {
                    if ((otherNFA.stateCount == 1)) {
                        newTransitionsTable[index].put(LAMBDA, mutableSetOf(thisNFA.stateCount))
                    } else {
                        transition.forEach { (_, state) ->
                            state.add(thisNFA.stateCount + 1)
                        }
                    }
                }
            }

            newTransitionsTable.addAll(otherNFA.transitionsTable.map { transition ->
                transition.mapValues { (_, state) ->
                    state.map { it + thisNFA.stateCount }.toMutableSet()
                }.toMutableMap()
            })

            return NFA(
                stateCount = thisNFA.stateCount + otherNFA.stateCount,
                transitionsTable = newTransitionsTable.map {
                    it.mapValues { (_, state) -> state.toSet() }.toMap()
                },
                startIndex = thisNFA.startIndex,
                terminalStates = otherNFA.terminalStates.map { states ->
                    states + thisNFA.stateCount
                }.toSet()
            ).toDFA()
        }

        fun toNFA(): NFA {
            return NFA(
                stateCount = stateCount,
                transitionsTable = transitionsTable.map {
                    it.mapValues { (_, state) -> setOf(state) }
                },
                startIndex = startIndex,
                terminalStates = terminalStates
            )
        }
        //endregion


        /**
         * Задача:
         * пересечение и объединение
         * Исполнитель: [A-13-17] Худяков Константин
         */
        //region dfa-intersection

        /**
         * Пересечение двух автоматов представляет из себя новый автомат, в котором:
         * 1) множество состояний - декартово произведение множеств состояний каждого автомата
         * 2) алфавит - объединением двух алфавитов
         * 3) начальная вершина S - вершина, соответствующая декартову произведению начальных вершин
         * 4) множество терминальных состояний - декартово произведение множеств терминальных состояний
         * 5) результирующая вершина при переходе по букве - декартово произведение результирующих вершин
         *      каждого автомата при переходе по этой букве
         */
        override fun intersect(other: DFA): DFA {
            return constructProduct(other, ::createIntersectionTerminalStates)
        }

        /**
         * Объединение двух автоматов представляет из себя новый автомат, в котором:
         * 1) множество состояний - декартово произведение множеств состояний каждого автомата
         * 2) алфавит - объединением двух алфавитов
         * 3) начальная вершина S - вершина, соответствующая декартову произведению начальных вершин
         * 4) множество терминальных состояний - объединение декартовых произведений множества
         *      терминальных состояний первого автомата на множество всех состояний второго
         *      и множества терминальных состояний второго автомата на множество всех состояний первого автомата
         * 5) результирующая вершина при переходе по букве - декартово произведение результирующих вершин
         *      каждого автомата при переходе по этой букве
         */
        override fun union(other: DFA): DFA {
            return constructProduct(other, ::createUnionTerminalStates)
        }

        private fun constructProduct(
            other: DFA,
            terminalStatesProducer: (Set<Int>, Set<Int>, Int, Int) -> Set<Int>
        ): DFA {
            val (first, second) = orderCommutative(this, other)
            val newStateCount = first.stateCount * second.stateCount
            val newAlphabet = first.alphabet.plus(second.alphabet)
            val newTransitionsTable = createDescartesTransitionsTable(first.transitionsTable, second.transitionsTable)
            val newStartIndex = getDescartesStateNum(first.startIndex, second.startIndex, second.stateCount)
            val newTerminalStates = terminalStatesProducer.invoke(
                first.terminalStates,
                second.terminalStates,
                first.stateCount,
                second.stateCount
            )

            return DFA(newStateCount, newAlphabet, newTransitionsTable, newStartIndex, newTerminalStates)
        }

        /**
         * Возвращает пару автоматов, в которой первым будет автомат с меньшем числом состояний
         * (необходима для того, чтобы операции объединения и пересечения были коммутативны по структуре автомата)
         */
        private fun orderCommutative(one: DFA, other: DFA): Pair<DFA, DFA> {
            return if (one.stateCount <= other.stateCount)
                one to other
            else other to one
        }

        /**
         * Преобразует номера состояний двух автоматов в один номер состояния в пересечнии/объединении этих автоматов
         */
        private fun getDescartesStateNum(first: Int, second: Int, secondSize: Int): Int = first * secondSize + second

        private fun createDescartesTransitionsTable(
            one: List<Map<Char, Int>>,
            other: List<Map<Char, Int>>
        ): List<Map<Char, Int>> {
            val stateCount = one.size * other.size
            return (0 until stateCount).map { curState ->
                val oneCurState = curState / other.size
                val otherCurState = curState % other.size
                one[oneCurState].keys
                    .intersect(other[otherCurState].keys)
                    .associateWith {
                        val first = one[oneCurState][it]!!
                        val second = other[otherCurState][it]!!
                        getDescartesStateNum(first, second, other.size)
                    }
            }.toList()
        }

        /**
         * Создаёт множество терминальных состояний, получаемое декартовым произведением множеств двух автоматов
         */
        private fun createIntersectionTerminalStates(
            one: Set<Int>,
            other: Set<Int>,
            oneStateCount: Int,
            otherStateCount: Int
        ): Set<Int> {
            return one.flatMap { terminalState ->
                other.map { terminalState to it }
            }.map { getDescartesStateNum(it.first, it.second, otherStateCount) }.toSet()
        }

        /**
         * Создаёт множество терминальных состояний, получаемое объединением декартовых произведений
         *      множества терминальных состояний первого автомата на множество всех состояний второго
         *      и множества терминальных состояний второго автомата на множество всех состояний первого автомата
         */
        private fun createUnionTerminalStates(
            one: Set<Int>,
            other: Set<Int>,
            oneStateCount: Int,
            otherStateCount: Int
        ): Set<Int> {
            val oldTerminalStates = one.flatMap { terminalState ->
                (0 until otherStateCount).map { terminalState to it }
            } union other.flatMap { terminalState ->
                (0 until oneStateCount).map { it to terminalState }
            }
            return oldTerminalStates.map { getDescartesStateNum(it.first, it.second, otherStateCount) }.toSet()
        }
        //endregion

        /**
         * Задача:
         * ДКА, принимающий реверсию всех слов языка текущего ДКА
         * Исполнитель: [А-13-17] Гвасалия Георгий
         */
        override fun reversed(): DFA {
            TODO("Not yet implemented")
        }

        enum class DFAMinimizationAlgorithm {
            Hopcroft, Moore, Brzozowski
        }

        fun minimized(algorithm: DFAMinimizationAlgorithm = DFAMinimizationAlgorithm.Hopcroft): DFA {
            return when (algorithm) {
                DFAMinimizationAlgorithm.Hopcroft -> hopcroftMinimized()
                DFAMinimizationAlgorithm.Moore -> mooreMinimized()
                DFAMinimizationAlgorithm.Brzozowski -> brzozowskiMinimized()
            }
        }

        /**
         * Задача:
         * алгоритм Брзощовски
         * Исполнитель: [A-13-17] Хабаров Павел
         */
        private fun brzozowskiMinimized(): DFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * алгоритм Мура
         * Исполнитель: [А-13-17] Новиков Александр
         */
        private fun mooreMinimized(): DFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * алгоритм Хопкрофта
         *Исполнитель: [А-05-17] Севостьянов Сергей
         */
        private fun hopcroftMinimized(): DFA {
            TODO("Not yet implemented")
        }

        fun toRightGrammar(): RegularGrammar.RightRegularGrammar = this.toNFA().toRightGrammar()

        fun toLeftGrammar(): RegularGrammar.LeftRegularGrammar = this.toNFA().toLeftGrammar()
    }


    /**
     * Задача:
     * реализовать NFA и nfa-basic
     * Исполнитель: [А-05-17] Морозов Степан
     */
    data class NFA(override val stateCount: Int,                    //Кол-во состояний
              val transitionsTable: List<Map<Char, Set<Int>>>,      //Таблица переходов (таблица смежности)
              val startIndex: Int,                             //Стартовая вершина
              val terminalStates: Set<Int>) :
        FiniteAutomaton<NFA>(stateCount) {

        companion object {
            /**
             * Задача:
             * генератор случайного НКА в подалфавите [a-z]
             * @param alphabetSize – размер целевого алфавита
             * @param hasLambda – допустимо ли использовать лямбда-переходы
             */
            fun generate(alphabetSize: Int, hasLambda: Boolean = true): NFA {
                TODO("Not yet implemented")
            }
        }

        //region nfa-basic
        override fun check(word: Word): Boolean {
            TODO("Not yet implemented")
        }

        override fun concatenate(other: NFA): NFA {
            TODO("Not yet implemented")
        }
        //endregion nfa-basic

        /**
         * Задача:
         * НКА, принимающий дополнение языка текущего НКА
         * и убрать тупики
         */
        override fun complement(): NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Преобразовать к НКА без лямбда-переходов
         * Исполнитель: [А-05-17] Кощин Евгений
         */
        fun removeLambdaTransitions(): NFA {
            TODO("Not yet implemented")
        }


        /**
         * Задача:
         * пересечение и объединение НКА
         * Исполнитель: [А-13-17] Шаронова Елена
         */
        //region nfa-intersection
        override fun intersect(other: NFA): NFA {
            TODO("Not yet implemented")
        }

        override fun union(other: NFA): NFA {
            TODO("Not yet implemented")
        }
        //endregion

        /**
         * Задача:
         * НКА, принимающий реверсию всех слов языка текущего НКА
         * Ограничение: единственное начальное состояние [Морозова Дарья А-13-17]
         */
        override fun reversed(): NFA {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * звезда Клини
         */
        //region nfa-closure
        override fun starClosure(): NFA {
            TODO("Not yet implemented")
        }

        override fun plusClosure(): NFA {
            TODO("Not yet implemented")
        }
        //endregion


        enum class NFAToRegExpConversionAlgorithm {
            Kleene,
            StateElimination
        }

        fun toRegExp(
            algorithm: NFAToRegExpConversionAlgorithm = NFAToRegExpConversionAlgorithm.StateElimination
        ): RegularExpression {
            return when (algorithm) {
                NFAToRegExpConversionAlgorithm.Kleene -> kleeneConversion()
                NFAToRegExpConversionAlgorithm.StateElimination -> stateElimination()
            }
        }

        /**
         * Задача:
         * Метод удаления состояний
         */
        private fun stateElimination(): RegularExpression {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * алгоритм Клини
         * @see https://en.wikipedia.org/wiki/Kleene%27s_algorithm
         */
        private fun kleeneConversion(): RegularExpression {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * алгоритм построения подмножеств
         * Исполнитель: [A-05-17] Коротаев Евгений
         */
        fun toDFA(): DFA {
            TODO("Not yet implemented")

        }


        /**
         * Задача:
         * Приведение к правой регулярной грамматике
         *	Исполнитель: [А-13-17] Маресина Александра
         */
        fun toRightGrammar(): RegularGrammar.RightRegularGrammar {
            TODO("Not yet implemented")
        }

        /**
         * Задача:
         * Приведение к левой регулярной грамматике
         *	Исполнитель: [А-13-17] Рощупкин Александр
         */
        fun toLeftGrammar(): RegularGrammar.LeftRegularGrammar {
            TODO("Not yet implemented")
        }

    }
}

/**
 * Задача
 * Алгоритм Ахо-Корасика
 *  @see https://en.wikipedia.org/wiki/Aho%E2%80%93Corasick_algorithm
 *	Исполнитель: [А-13-17] Полянский Родион
 */
//region aho-corasick

/**
 * Алгоритм Ахо-Корасик реализует поиск мн-ва паттернов из списка в данной строке.
 * Результат - отображение: индекс из данной строки -> список паттернов из списка, начинающаяся с этого индекса.
 */
fun ahoCorasick(patterns: List<String>, text: String): Map<Int, List<String>> {
    return AhoCorasick(Bor.of(patterns)).processText(text)
}

data class Bor(
    val patterns: List<String>,                      // переданные паттерны строк
    val nodes: List<BorNode>                         // список узлов бора
) {
    data class BorNode(
        val charToParent: Char,                      // символ, ведущий к родителю
        val parent: Int? = null,                     // индекс родителя
        val nextNodes: Map<Char, Int> = mapOf(),     // выходящие узлы
        val patternNum: Int? = null,                 // номер паттерна, за который отвечает терминал
        val isLeaf: Boolean = false                  // флаг, является ли вершина терминалом
    )

    companion object {
        fun of(patterns: List<String>): Bor {
            var nodes = listOf(BorNode('$'))

            for (i in 0 until patterns.size) {
                nodes = addPattern(nodes, patterns[i], i)
            }

            return Bor(patterns, nodes)
        }

        const val ROOT = 0

        private fun addPattern(nodes: List<BorNode>, str: String, patternNum: Int): List<BorNode> {
            var node = ROOT
            val newNodes = nodes.toMutableList()

            for (ch in str) {
                if (!newNodes[node].nextNodes.containsKey(ch)) {
                    newNodes.add(BorNode(charToParent = ch, parent = node))

                    val newNextNodes = newNodes[node].nextNodes.toMutableMap()
                    newNextNodes[ch] = newNodes.size - 1

                    newNodes[node] = newNodes[node].copy(nextNodes = newNextNodes)
                }

                node = newNodes[node].nextNodes[ch]!!
            }

            newNodes[node] = newNodes[node].copy(isLeaf = true, patternNum = patternNum)

            return newNodes
        }
    }
}

/**
 * Алгоритм Ахо-Корасик состоит из 3 шагов:
 * 1) Построение бора из переданных паттернов.
 * 2) Построение суффиксных ссылок.
 * 3) Построение сжатых суффиксных ссылок.
 */
class AhoCorasick(private val bor: Bor) {
    // массив из переходов, используемых для вычисления суффиксных ссылок
    private var suffNodes: Array<MutableMap<Char, Int>> = Array(bor.nodes.size) { mutableMapOf<Char, Int>() }
    private var suffLinks: Array<Int?> = arrayOfNulls(bor.nodes.size)           // массив суффиксных ссылок
    private var comprSuffLinks: Array<Int?> = arrayOfNulls(bor.nodes.size)      // массив сжатых суффиксных ссылок

    /**
     * Получает суффиксную ссылку, т.е. для вершины v это указатель на вершину u, в котором
     * строка в u - наибольший cобственный суффикс строки v.
     */
    private fun getSuffLink(ind: Int?): Int? {
        val node = bor.nodes[ind!!]

        if (suffLinks[ind] == null) {
            if (ind == Bor.ROOT || node.parent == Bor.ROOT) {
                suffLinks[ind] = Bor.ROOT
            } else {
                suffLinks[ind] = getLink(getSuffLink(node.parent), node.charToParent)
            }
        }

        return suffLinks[ind]
    }

    /**
     * Реализует функцию перехода по символу из текущего состояния:
     * 1) если есть возможность перейти по символу, то делаем это;
     * 2) иначе, переходим по суффиксной ссылке и там делаем переход по символу.
     *
     * (суффиксная ссылка корня указывает на корень.)
     */
    private fun getLink(ind: Int?, ch: Char): Int {
        val node = bor.nodes[ind!!]

        if (!suffNodes[ind].containsKey(ch)) {
            suffNodes[ind][ch] = when {
                node.nextNodes[ch] != null && node.nextNodes[ch]!! > 0 -> node.nextNodes[ch]!!
                ind == Bor.ROOT -> Bor.ROOT
                else -> getLink(getSuffLink(ind), ch)
            }
        }

        return suffNodes[ind][ch]!!
    }

    /**
     * Вычисляет сжатую суффиксную ссылку, т.е. ближайший терминал перехода по суффиксным ссылкам.
     *
     * (сжатая суффиксная ссылка корня указывает на корень.)
     */
    private fun getComprSuffLink(ind: Int?): Int? {
        if (comprSuffLinks[ind!!] == null) {

            comprSuffLinks[ind] = when {
                bor.nodes[getSuffLink(ind)!!].isLeaf -> getSuffLink(ind)
                getSuffLink(ind) == Bor.ROOT -> Bor.ROOT
                else -> getComprSuffLink(getSuffLink(ind))
            }
        }

        return comprSuffLinks[ind]
    }

    /**
     * Ищет совпадения с паттернами, проходясь по сжатым суффиксным ссылкам.
     */
    private fun checkMatching(
        node: Int?,
        indInText: Int,
        match: MutableMap<Int, MutableList<String>>
    ) {
        var u = node

        while (u != Bor.ROOT) {
            if (bor.nodes[u!!].isLeaf) {
                val pattern = bor.patterns[bor.nodes[u].patternNum!!]
                val ind = indInText - pattern.length + 1

                if (match[ind] == null) {
                    match[ind] = mutableListOf()
                }

                match[ind]?.add(pattern)
            }

            u = getComprSuffLink(u)
        }
    }

    /**
     * Реализует поиск паттернов в переданной строке.
     */
    fun processText(text: String): Map<Int, List<String>> {
        var node = Bor.ROOT
        val match = mutableMapOf<Int, MutableList<String>>()

        for (i in 0 until text.length) {
            node = getLink(node, text[i])
            checkMatching(node, i + 1, match)
        }

        return match
    }

}

//endregion aho-corasick

/**
 * Задача
 * Алгоритм Бертранда, альтернатива Ахо-Карасику
 * @see http://se.ethz.ch/~meyer/publications/string/string_matching.pdf
 *  Исполнитель: [А-13-17] Набиева Ольга
 */
fun bertrand(/*TODO*/) {
    TODO("Not yet implemented")
}
